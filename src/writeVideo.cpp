#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

using namespace std;

cv::VideoWriter writer;

void writeVideoCallBack(const sensor_msgs::ImageConstPtr &image)
{
    cv_bridge::CvImagePtr bridge;
    try
    {
        bridge=cv_bridge::toCvCopy(image,"bgr8");
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("Failed to transform color image.");
        return;
    }

    cv::Mat img = cv::Mat(bridge->image);

    writer.write(img);

    return ;
}

int main(int argc, char **argv)
{
    writer.open("droneVideo.avi",CV_FOURCC('M','J','P','G'),10,cv::Size(640,360),true);

    ros::init(argc,argv,"writeVideo");

    ros::NodeHandle nVideo;

    ros::Subscriber subVideo = nVideo.subscribe("/ardrone/image_raw",3,writeVideoCallBack);

    ros::spin();

    writer.release();

    return 0;
}

/*
 * Date 20 April, 2015
 * This code is written for retail stock assessment using images.
 * It recognize and count the products in the images. It also gives a
 * availability map of the stocks in the retail store. All the product templates are stored
 * before running the code instance. These templates are used for product identification.
 *
 * Input : RGB images on any topic
 *          (Currently it is subscribing /ardrone/image_raw topic. You can change it to any topic)
 * Output : A image window will be shown having an output of product counts and availbility map.
 */

#include <iostream>
#include <productCount/productCount.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/String.h>
#include <cv_bridge/cv_bridge.h>


using namespace std;

void productCountCallBack(const sensor_msgs::ImageConstPtr& image, ros::Publisher& pubProductCount)
{
    cv_bridge::CvImagePtr bridge;
    try
    {
        bridge=cv_bridge::toCvCopy(image,"bgr8");
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("Failed to transform color image.");
        return;
    }

    cv::Mat img = cv::Mat(bridge->image);

    vector<int> countsOnProducts(int(NO_OF_TEMPLATES));

    cv::Mat productOutputImage;

    cout << "Inside callback" << endl;

    ros::Time time1 = ros::Time::now();

    identifyAndCountProductsInImage(img,countsOnProducts,productOutputImage);


    cout << (ros::Time::now()-time1).toSec() << endl;

    cv::imshow("image",productOutputImage);
    cv::waitKey(1);


//    cout << countsOnProducts.size() << endl;
//    for(int i=0;i<countsOnProducts.size();i++)
//        cout << countsOnProducts[i] << "\t" << i << endl;

//    std_msgs::String resultString;
//    std::string rString;
//    for(int i=0;i<countsOnProducts.size();i++)
//    {
//        char s[100];
//        sprintf(s,"%d",countsOnProducts[i]);

//        rString.append(s);
//    }

//    resultString.data = rString;
    return ;
}

int main(int argc, char **argv)
{
    ros::init(argc,argv,"productCount");

    ros::Publisher pubProductCount;

    templateKeypoints = vector< vector<cv::KeyPoint> >(int(NO_OF_TEMPLATES));
    emModelTemplate = vector<cv::EM>(int(NO_OF_TEMPLATES));
    initProductCount(imgFileNames,descriptorNodeTable,keyPointNodeTable,dataSet,descriptorCount,majorMinorAxesOfTemplates
                         ,ellipseTemplates,templateHistograms,templateKeypoints,templateDescriptors,emModelBG,emModelTemplate
                         ,totalProductsDetected);


    ros::NodeHandle nImage;
    ros::NodeHandle nPub;

    ros::Subscriber subImage = nImage.subscribe<sensor_msgs::Image>("/ardrone/image_raw",3,boost::bind(productCountCallBack,_1,
                                                                                   boost::ref(pubProductCount)));

    pubProductCount = nPub.advertise<std_msgs::String>("/productVector",1);
    pubProductCount.publish(std_msgs::String());

    ros::spin();

    return 0;
}

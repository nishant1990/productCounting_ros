System requirements :
	OS : Ubuntu 14.04
	ROS : hydro or indigo
	Other requirements : Opencv 2.4.9

Input : 
	1. Image topic(RGB Image)
Output : 
	1. An output having information about products and availability map.

Note : All the product templates are stored in a separate folder.

#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <surflib.h>

using namespace std;

#define RANSAC_THRESHOLD    1   /*!< Ransac threshold */
#define JUNK_OUTPUT         0
#define POINT_RECT          0
#define NO_OF_TEMPLATES                 24      /*!< Number of products in the store */
#define HESSIAN_THRESHOLD               100     /*!< Hessian threshold for feature detection (Larger the value ,lesser the number of features detected )*/
#define DISTANCE_RATIO                  0.9     /*!< Distance ratio threshold :: If a nearest neighbour searched through kdtree is lower
    than this threshold then it is a good match otherwise not */
#define JUNK_OUTPUT                     0       /*!< This flag is set to debug the code */
#define MIN_POINTS_FOR_OBJECT           8       /*!< Minimum number of points to recognize anb object */
#define FRAME_COLS                      640     /*!< Number of columns in the frame captured through video camera */
#define FRAME_ROWS                      480     /*!< Number of rows in the frame captured through video camera */
#define FREQUENCY_THRESHOLD             0.10    /*!< Minimum Threshold on frequency to consider it correct*/
#define MAX_SINGLE_PRODUCT_LIMIT        10      /*!< Maximum number of products of a particular type in a frame */
#define OBJECT_THRESHOLD                0.1     /*!< Minimum threshold on matched descriptor for considering it as an object */
#define SHOW_AVAILABILITY_MAP           1       /*!< Results showing prodcut availability map in the result window  */
#define TOTAL_FRAMES                    316     /*!< Total number of frames in the video   */
#define FONTSIZE                        1
#define OPENCV_FEATURE_DETECTOR         0

// LibDAI
#include<dai/bp.h>
#include<dai/factorgraph.h>

#include<ros/ros.h>

// GMM Parameters
#define NUM_BG_TRAIN_IMAGES 10
#define DIMS                3
#define GMM_CLUSTERS        5
#define GMM_TRAINED         1
#define PRODUCT_THRESHOLD   50
#define GRID                5

// DEBUG Parameters
//#define DEBUG_FF            1
//#define DEBUG_SURF          1
//#define DEBUG_PM            1
//#define DEBUG_NBR           1


cv::Mat imgMeansTemp;
cv::VideoWriter writer, writer2;

vector<string> imgFileNames;
std::map<int , vector<int> > descriptorNodeTable;
std::map<int, cv::KeyPoint> keyPointNodeTable;
cv::Mat dataSet;
vector<int> descriptorCount;
vector<cv::Point2f> majorMinorAxesOfTemplates;
vector<cv::RotatedRect> ellipseTemplates;
vector<cv::Mat> templateHistograms;
vector< vector<cv::KeyPoint> > templateKeypoints;
vector<  cv::Mat > templateDescriptors;
cv::EM emModelBG;
vector<cv::EM> emModelTemplate;
vector<int> totalProductsDetected;
cv::flann::Index index1;


#define PRODUCT_RECO_GMM    0
#define COLOR_GRAPH         0
#define GMM_DECIDE_ROI      0
/*!
  @brief Trains the Foreground GMM model.
  @param[in]     trainImg   Input image for training the model.
  @param[in,out] emModel    Input Model to be trained.
  */
void trainGMMfg(cv::Mat trainImg, cv::EM& emModel)
{
    cv::Mat sampleInput;

    if(trainImg.empty())
    {
        cout << "Foreground Train Image not found, Exiting" << endl;
        exit(-1);
    }

    cvtColor(trainImg, trainImg, CV_BGR2HSV);

    for(int j=0; j<trainImg.cols; j++)
        for(int k=0; k<trainImg.rows; k++)
        {
            cv::Mat temp2(1,DIMS,CV_64FC1);

            temp2.at<double>(0,0) = double(trainImg.at<cv::Vec3b>(k,j)[0]);
            temp2.at<double>(0,1) = double(trainImg.at<cv::Vec3b>(k,j)[1]);
            temp2.at<double>(0,2) = double(trainImg.at<cv::Vec3b>(k,j)[2]);

            sampleInput.push_back(temp2.row(0));
        }

    cout << "Sample Pixels = " << sampleInput.rows << endl;

    emModel.train(sampleInput);
}


/*!
  @brief Trains the Background GMM model.
  @param[in,out] emModel    Input Model to be trained.
  */
void trainGMMbg(cv::EM& emModel)
{
    cv::Mat sampleInput;

    int frameCounter=0;

    for(int k=0; k<NUM_BG_TRAIN_IMAGES; k++)
    {
        cv::Mat trainImg;

        frameCounter++;

        char fname[300];
//        sprintf(fname,"../train/bg/%d.png", k );
        sprintf(fname,"/opt/rce/packages/productcount_ros/train/bg/%d.png", k );
        trainImg = cv::imread(fname);

        if(trainImg.empty())
        {
            cout << "Background Train Image not found at given location,'" << fname << "' Exiting" << endl;
            exit(-1);
        }

        cv::Mat trainImgRoi = trainImg.clone();

        cv::cvtColor(trainImgRoi, trainImgRoi, CV_BGR2HSV);

        for(int j=0; j<trainImgRoi.cols; j++)
            for(int k=0; k<trainImgRoi.rows; k++)
            {

                cv::Mat temp2(1,DIMS,CV_64FC1);

                temp2.at<double>(0,0) = double(trainImgRoi.at<cv::Vec3b>(k,j)[0]);
                temp2.at<double>(0,1) = double(trainImgRoi.at<cv::Vec3b>(k,j)[1]);
                temp2.at<double>(0,2) = double(trainImgRoi.at<cv::Vec3b>(k,j)[2]);

                sampleInput.push_back(temp2.row(0));
            }

    }

    cout << "Sample Pixels = " << sampleInput.rows << endl;

    emModel.train(sampleInput);

    cv::FileStorage bgModel("/opt/rce/packages/productcount_ros/models/bgModel.yaml",cv::FileStorage::WRITE);
    emModel.write(bgModel);
    bgModel.release();

    cout << "Training Completed for Background " << endl;
}



/*!
  @brief Calculates probability for a given Multivariate Gaussian Distribution parameters.
  @param[in] clusters   Number of gaussian components/clusters.
  @param[in] dims       Dimension of input data.
  @param[in] weights    Weights calculated for the model.
  @param[in] means      Means calculated for the model.
  @param[in] covs       Covariance Matrices calculated for the model.
  @param[in] covsInv    Inverse Covariance Matrices.
  @param[in] sample     Input data for which probability has to be calcualted.
  @return   Probability value i.e. the belongingness of that data point to the mixture model.
  */
double calcModel(int clusters, int dims, cv::Mat weights, cv::Mat means, vector<cv::Mat> covs, vector<cv::Mat> covsInv, cv::Mat sample)
{
    double prob = 0;

    sample.convertTo(sample, CV_64FC1);

    for(int i=0; i<clusters; i++)
    {
        cv::Mat deviation = sample.t() - means.row(i).t();
        cv::Mat val = deviation.t() * covsInv[i] * deviation;
        double divFactor = 0;

        if(determinant(covs[i]) != 0)
            divFactor = pow((44/7),dims/2) * sqrt( determinant(covs[i]) );
        else
        {
            divFactor = pow((44/7),dims/2);
        }

        prob += ( weights.at<double>(0,i) *  exp( -0.5 * val.at<double>(0,0) ) / divFactor );
    }

    return prob;
}


/*!
  @brief Finds out if shelf is empty, and finds out the percentage empty region.
  @param[in] input      Input Image.
  @param[in] pt1        Point to be tested.
  @param[in] emModelFG  Foreground GMM model.
  @param[in] emModelBG  Background GMM model.
  @return   Flag is true if shelf is empty and false otherwise.
  */
bool pointRecognise(cv::Mat input, cv::Point2f pt1,  cv::EM emModelFG, cv::EM emModelBG)
{
    cv::Mat queryImg = input.clone();
    //    cvtColor(input, queryImg, CV_BGR2HSV);

    // Get parameters for Foreground Model
    cv::Mat weights = emModelFG.get<cv::Mat>("weights");
    cv::Mat means = emModelFG.get<cv::Mat>("means");
    vector<cv::Mat> covs = emModelFG.get<vector <cv::Mat> >("covs");

    vector<cv::Mat> covsInv;
    for(unsigned int k=0; k<covs.size(); k++)
        covsInv.push_back( (covs[k]).inv());

    // Get parameters for Background Model
    cv::Mat weights2 = emModelBG.get<cv::Mat>("weights");
    cv::Mat means2 = emModelBG.get<cv::Mat>("means");
    vector<cv::Mat> covs2 = emModelBG.get<vector <cv::Mat> >("covs");

    vector<cv::Mat> covsInv2;
    for(unsigned int k=0; k<covs2.size(); k++)
        covsInv2.push_back( (covs2[k]).inv());



    cv::Mat sample(1,DIMS,CV_64FC1);
    sample.at<double>(0,0) = queryImg.at<cv::Vec3b>(pt1.y,pt1.x)[0];
    sample.at<double>(0,1) = queryImg.at<cv::Vec3b>(pt1.y,pt1.x)[1];
    sample.at<double>(0,2) = queryImg.at<cv::Vec3b>(pt1.y,pt1.x)[2];

    double probability = 0;
#if(USE_OCV_EM_PRED)

    cv::Mat probs, probs2;
    cv::Vec2d outBG = emModelBG.predict(sample.row(0), probs2);
    cv::Vec2d outFG = emModelFG.predict(sample.row(0), probs);
    probability = exp(outFG[0])/(exp(outFG[0])+exp(outBG[0]));

#else
    double prob1 = calcModel(weights.cols, DIMS, weights, means, covs, covsInv, sample  );
    double prob2 = calcModel(weights2.cols, DIMS, weights2, means2, covs2, covsInv2, sample  );
    probability = prob1/(prob1+prob2);
#endif

    if(probability > 0.5)
    {
        return true;
    }
    else
    {
        return false;
    }
}


/*!
  @brief Finds out if shelf is empty, and finds out the percentage empty region.
  @param[in] input      Query Image.
  @param[in] emModelFG  Foreground GMM model.
  @param[in] emModelBG  Background GMM model.
  @param[out] percentBg Percentage of empty region.
  @param[out] outImg    Foreground-Background Segmented Image.
  @return   Flag is true if shelf is empty and false otherwise.
  */
bool productRecognise(cv::Mat input,  cv::EM emModelFG, cv::EM emModelBG, float& percentFG , cv::Mat& outImg, cv::Rect& boundRect)
{
    //    cv::imshow("prod1",input);
    cv::Mat queryImg;
    cvtColor(input, queryImg, CV_BGR2HSV);

    // Get parameters for Foreground Model
    cv::Mat weights = emModelFG.get<cv::Mat>("weights");
    cv::Mat means = emModelFG.get<cv::Mat>("means");
    vector<cv::Mat> covs = emModelFG.get<vector <cv::Mat> >("covs");

    vector<cv::Mat> covsInv;
    for(unsigned int k=0; k<covs.size(); k++)
        covsInv.push_back( (covs[k]).inv());

    // Get parameters for Background Model
    cv::Mat weights2 = emModelBG.get<cv::Mat>("weights");
    cv::Mat means2 = emModelBG.get<cv::Mat>("means");
    vector<cv::Mat> covs2 = emModelBG.get<vector <cv::Mat> >("covs");

    vector<cv::Mat> covsInv2;
    for(unsigned int k=0; k<covs2.size(); k++)
        covsInv2.push_back( (covs2[k]).inv());

    cv::Mat samplesQ;
    outImg = input.clone();//cv::Mat::zeros(queryImg.rows, queryImg.cols, CV_8UC3);
    vector<cv::Point2f> inPoints1;
    int emptyShelfCounter = 0;
    int totalCounter=0;

    for(int j=0; j<queryImg.cols-GRID; j+=GRID)
        for(int k=0; k<queryImg.rows-GRID; k+=GRID)
        {
            cv::Rect roi = cv::Rect(j,k,GRID,GRID);
            cv::Mat grid = queryImg(roi).clone();
            cv::Scalar meanVal = mean(grid);

            cv::Mat temp2(1,DIMS,CV_64FC1);
            temp2.at<double>(0,0) = meanVal[0];
            temp2.at<double>(0,1) = meanVal[1];
            temp2.at<double>(0,2) = meanVal[2];

            samplesQ.push_back(temp2.row(0));

            double probability = 0;
#if(USE_OCV_EM_PRED)

            cv::Mat probs, probs2;
            cv::Vec2d outBG = emModelBG.predict(temp2.row(0), probs2);
            cv::Vec2d outFG = emModelFG.predict(temp2.row(0), probs);
            probability = exp(outBG[0])/(exp(outFG[0])+exp(outBG[0]));

#else
            double prob1 = calcModel(weights.cols, DIMS, weights, means, covs, covsInv, temp2  );
            double prob2 = calcModel(weights2.cols, DIMS, weights2, means2, covs2, covsInv2, temp2  );
            probability = prob2/(prob1+prob2);
#endif
            if(probability > 0.5)
            {
                emptyShelfCounter++;
            }
            else
            {
                outImg.at<cv::Vec3b>(k,j) = cv::Vec3b(0,0,255);
                inPoints1.push_back(cv::Point2f(j,k));
            }
            totalCounter++;
        }

    if(!inPoints1.empty())
        boundRect = cv::boundingRect(inPoints1);
    else
        boundRect = cv::Rect(0,0,0,0);

    bool productDetected;

    percentFG = 1 - (float)emptyShelfCounter/(float)totalCounter;

    float productThresh = float(PRODUCT_THRESHOLD)/100.0;

    if(percentFG > productThresh)
    {
        productDetected = true;
    }
    else
    {
        productDetected = false;
    }

    //    cv::imshow("prod2",outImg);
    return productDetected;
}



void weightScalingAspect( vector<cv::Point2f>& matchPoints1, vector<cv::Point2f>& matchPoints2, double *overallScale,int sr)
{
    int SCALE_RANGE = sr;
    int count=0, ind=0, num = matchPoints1.size();
    vector<double> scale;

    if (matchPoints1.size() > 1 )
    {
        for (int i = 0; i < num; i++)
            for (int j=i+1; j< num; j++)
            {

                if ((matchPoints1[i].x - matchPoints1[j].x) != 0 && (matchPoints1[i].y - matchPoints1[j].y) != 0)
                {
                    scale.push_back(norm(matchPoints2[i] - matchPoints2[j])/norm(matchPoints1[i] - matchPoints1[j]));

                    //                    if ( scale[count]< (1+float(SCALE_RANGE)/100) && scale[count] > (1-float(SCALE_RANGE)/100) )
                    if ( scale[count]< (float(SCALE_RANGE)/100.0) && scale[count] > 1/(float(SCALE_RANGE)/100.0) )
                    {
                        // scaleData << count<<"\t"<<scale[count]<<"\t"<<weight[count]<<"\t"<<matchPoints1[i]<<"\t"<<matchPoints1[j]<<"\t"<<matchPoints2[i]<<"\t"<<matchPoints2[j]<<endl;
                        count++;
                    }
                    else
                        scale.pop_back();
                }
            }


        *overallScale=0;

        // Computing overall scaling for the new ROI
        for (int i=0; i<count; i++ )
            *overallScale += scale[i];

        if(count > 1)
            *overallScale /= count;
        else
            *overallScale =1;

    }
    else
    {
        cout << "MatchPoints less than 3 while calculating Scaling" << endl;
        *overallScale=1;
    }
}
double printAngle(cv::RotatedRect calculatedRect){
    if(calculatedRect.size.width < calculatedRect.size.height){
        return(calculatedRect.angle+180);
    }else{
        return(calculatedRect.angle+90);
    }
}
void drawRectangle(cv::Mat& img, vector<cv::Point2f> roiPoints, cv::Scalar color, int thickness=1 )
{
    if(roiPoints.size() == 0)
        cerr << " Cannot draw roi as the vector is empty." << endl;

    for(int k=0; k<roiPoints.size()-1; k++)
    {
        cv::line(img,roiPoints[k],roiPoints[k+1], color, thickness, CV_AA);
    }

    cv::line(img,roiPoints[3],roiPoints[0], color, thickness, CV_AA);
}
/**
 * Rotate an image
 */
void rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
    int len = std::max(src.cols, src.rows);
    cv::Point2f pt(len/2., len/2.);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);

    cv::warpAffine(src, dst, r, cv::Size(len, len));
}
/*!
  @brief This function is used for descriptor matching.
  @param descriptorMatcher Abstract class for matching keypoint descriptors
  @param descriptors1   Set of descriptors of first image
  @param descriptors2   Set of descriptors of second image
  @param filteredMatches12  Class for matching keypoint descriptors
  @param knn    K-nearest neighbour
  @return void
  */

void crossCheckMatching( cv::Ptr<cv::DescriptorMatcher>& descriptorMatcher,
                         const cv::Mat& descriptors1, const cv::Mat& descriptors2,
                         vector<cv::DMatch>& filteredMatches12, int knn=1 )
{
    filteredMatches12.clear();
    assert(!descriptors1.empty());
    vector<vector<cv::DMatch> > matches12, matches21;
    descriptorMatcher->knnMatch( descriptors1, descriptors2, matches12, knn );
    descriptorMatcher->knnMatch( descriptors2, descriptors1, matches21, knn );
    for( size_t m = 0; m < matches12.size(); m++ )
    {
        bool findCrossCheck = false;
        for( size_t fk = 0; fk < matches12[m].size(); fk++ )
        {
            cv::DMatch forward = matches12[m][fk];

            for( size_t bk = 0; bk < matches21[forward.trainIdx].size(); bk++ )
            {
                cv::DMatch backward = matches21[forward.trainIdx][bk];
                if( backward.trainIdx == forward.queryIdx )
                {
                    filteredMatches12.push_back(forward);
                    findCrossCheck = true;
                    break;
                }
            }
            if( findCrossCheck ) break;
        }
    }
}

/*!
  @brief This function finds the surf matching between two images.
  @param srcDesc    Set of descriptors of source image
  @param dstDesc    Set of descriptors of destination image
  @param srcKeyPoint    Set of keypoints of source image
  @param dstKeyPoint    Set of keypoints of destination image
  @return percentage matching between two set of descriptors
  */
double surfMatching(cv::Mat& srcDesc, cv::Mat& dstDesc, vector<cv::KeyPoint>& srcKeyPoint, vector<cv::KeyPoint>& dstKeyPoint,
                    cv::Mat &img1,cv::Mat& img2, cv::Point2f &cPoint, cv::Rect& roi)
{
    double percentageMatch = 0.0;

    vector<int> queryIdxs,  trainIdxs;
    vector<cv::DMatch> filteredMatches;


    cv::Ptr<cv::DescriptorMatcher> descriptorMatcher;
    descriptorMatcher = cv::DescriptorMatcher::create( "FlannBased" );


    if(srcDesc.rows < 1 || dstDesc.rows < 1)
        return double(0.0);

    crossCheckMatching( descriptorMatcher, srcDesc, dstDesc, filteredMatches, 1 );

    for( size_t i = 0; i < filteredMatches.size(); i++ )
    {
        queryIdxs.push_back(filteredMatches[i].queryIdx);
        trainIdxs.push_back(filteredMatches[i].trainIdx);
    }

    cv::Mat pointsTransed; // Points for inverse homography
    cv::Mat H12;

    if(filteredMatches.size() < 5)
    {
        return double(0.0);
    }

    if( RANSAC_THRESHOLD >= 0 )
    {
        vector<cv::Point2f> points1; cv::KeyPoint::convert(srcKeyPoint, points1, queryIdxs);
        vector<cv::Point2f> points2; cv::KeyPoint::convert(dstKeyPoint, points2, trainIdxs);
        H12 = cv::findHomography( cv::Mat(points1), cv::Mat(points2), CV_RANSAC, RANSAC_THRESHOLD );
    }

    cv::Mat drawImg;


    if( !H12.empty() )
    {
        vector<char> matchesMask( filteredMatches.size(), 0 );
        vector<cv::Point2f> points1; cv::KeyPoint::convert(srcKeyPoint, points1, queryIdxs);
        vector<cv::Point2f> points2; cv::KeyPoint::convert(dstKeyPoint, points2, trainIdxs);

        cv::Mat points1t; cv::perspectiveTransform(cv::Mat(points1), points1t, H12);


        int count = 0;
        for( size_t i1 = 0; i1 < points1.size(); i1++ )
        {
            if( ( norm(points2[i1] - points1t.at<cv::Point2f>((int)i1,0)) <= 20 ))
            {
                count++;
                matchesMask[i1] = 1;
            }
        }

        //            cout << "Number of matches \t" <<  count << endl;
        percentageMatch = (double)(count*100)/(srcDesc.rows);


        // Transform region points
        vector<cv::Point2f> centerVec;
        cv::Point2f centerSURF(img1.cols/2,img1.rows/2);
        cv::Mat centerVecOut;
        centerVec.push_back(centerSURF);
        centerVec.push_back(cv::Point2f(0,0));

#if(POINT_RECT)
        centerVec.push_back(cv::Point2f(img1.cols,0));
#endif

        centerVec.push_back(cv::Point2f(img1.cols,img1.rows));

#if(POINT_RECT)
        centerVec.push_back(cv::Point2f(0,img1.rows));
#endif

        perspectiveTransform((cv::Mat)centerVec, centerVecOut, H12);
        centerSURF = centerVecOut.at<cv::Point2f>(0);


#if(POINT_RECT)
        for(int k1=1; k1<centerVecOut.rows; k1++)
            roiPoints.push_back(centerVecOut.at<cv::Point2f>(k1));
#else

        roi.x = centerVecOut.at<cv::Point2f>(1).x;
        roi.y = centerVecOut.at<cv::Point2f>(1).y;
        roi.width = abs(centerVecOut.at<cv::Point2f>(2).x - centerVecOut.at<cv::Point2f>(1).x);
        roi.height = abs(centerVecOut.at<cv::Point2f>(2).y - centerVecOut.at<cv::Point2f>(1).y);


#endif
        cPoint = centerSURF;
        //        cPoint = cv::Point2f(roi.x+roi.width/2,roi.y+roi.height/2);

        roi.x = cPoint.x - roi.width/2;
        roi.y = cPoint.y - roi.height/2;

        drawMatches( img1, srcKeyPoint, img2, dstKeyPoint, filteredMatches, drawImg, CV_RGB(0, 255, 0), CV_RGB(0, 0, 255), matchesMask
             #if DRAW_RICH_KEYPOINTS_MODE
                     , DrawMatchesFlags::DRAW_RICH_KEYPOINTS
             #endif
                     );
    }
    cv::Rect roi2 = roi;

    roi2.x += img1.cols;
    cv::rectangle(drawImg,roi2,cv::Scalar(0,255,0),2);

#if(DEBUG_SURF)
    cv::imshow("draw",drawImg);
    cv::waitKey(0);
#endif
    return percentageMatch;
}

/*!
  @brief This function appends a matrix vertically
  @param originalMat    Matrix to which image will be appended
  @param matToBeAppended    Matrix to be appended
  @return Void
  */

void appendMatrix( cv::Mat &originalMat,const cv::Mat& matToBeAppended )
{


    if(! originalMat.empty() )
    {
        assert( originalMat.cols == matToBeAppended.cols ) ;
        assert( originalMat.type() == matToBeAppended.type() ) ;


        cv::Mat newTemp( originalMat.rows+ matToBeAppended.rows , matToBeAppended.cols,matToBeAppended.type() ) ;

        int i ;
        for( i=0;i< originalMat.rows ; i++)
        {
            cv::Mat rowI = newTemp.row(i) ;
            //originalMat.row(i).copyTo( newTemp.row(i) );
            originalMat.row(i).copyTo( rowI );

        }
        for(int j=0; j< matToBeAppended.rows ; j++ )
        {
            cv::Mat rowJpluI =  newTemp.row( j+i ) ;
            //matToBeAppended.row(j).copyTo( newTemp.row( j+i ) );
            matToBeAppended.row(j).copyTo( rowJpluI );
        }

        originalMat = newTemp ;

    }
    else
    {
        originalMat = matToBeAppended ;
    }
    return ;

}

/*!
  @brief This function appends a matrix horizontally
  @param originalMat    Matrix to which image will be appended
  @param matToBeAppended    Matrix to be appended
  @return Void
  */
// Append matrix horizontally
void appendMatrixHorz( cv::Mat &originalMat,const cv::Mat& matToBeAppended )
{


    if(! originalMat.empty() )
    {
        assert( originalMat.rows == matToBeAppended.rows ) ;
        assert( originalMat.type() == matToBeAppended.type() ) ;


        cv::Mat newTemp( originalMat.rows ,originalMat.cols + matToBeAppended.cols,matToBeAppended.type() ) ;

        int i ;
        for( i=0;i< originalMat.cols ; i++)
        {
            cv::Mat colI = newTemp.col(i) ;
            //originalMat.row(i).copyTo( newTemp.row(i) );
            originalMat.col(i).copyTo( colI );

        }
        for(int j=0; j< matToBeAppended.cols ; j++ )
        {
            cv::Mat colJpluI =  newTemp.col( j+i ) ;
            //matToBeAppended.row(j).copyTo( newTemp.row( j+i ) );
            matToBeAppended.col(j).copyTo( colJpluI );
        }

        originalMat = newTemp ;

    }
    else
    {
        originalMat = matToBeAppended ;
    }
    return ;

}

bool verifyResultantRoi(cv::Rect r1, cv::Rect r2, float& ARerr, float& scale, bool rotateFlag = false)
{
    float ARnow = 0;
    scale = 0;

    if(!rotateFlag)
    {
        ARnow = float(r2.width) / float(r2.height);
        scale = float(r2.width) / float(r1.width);
    }
    else
    {
        ARnow = float(r2.height) / float(r2.width);
        scale = float(r2.height) / float(r1.width);
    }

    float AR = float(r1.width) / float(r1.height);

    ARerr = AR/ARnow;

    //    cout << "err\t" << ARerr << "\t" << scale << endl;
    if(ARerr > 1.3 || ARerr < 0.7)
    {
        return false;
    }

    if(scale < 0.3 || scale > 3 )
    {
        return false;
    }

    return true;
}

/*!
  @brief Finds out overlap between the given rectangles in an image.
  @param[in] tl1 Top left corner of rectangle 1.
  @param[in] tl2 Top left corner of rectangle 2.
  @param[in] sz1 Size of rectangle 1.
  @param[in] sz2 Size of rectangle 1.
  @param[out] overlap   Overlapping area in pixels.
  */
void calcOverlap(cv::Point tl1, cv::Point tl2, cv::Size sz1, cv::Size sz2, float *overlap)
{
    cv::Rect roi;

    int x_tl = max(tl1.x, tl2.x);
    int y_tl = max(tl1.y, tl2.y);
    int x_br = min(tl1.x + sz1.width, tl2.x + sz2.width);
    int y_br = min(tl1.y + sz1.height, tl2.y + sz2.height);
    if (x_tl < x_br && y_tl < y_br)
        roi = cv::Rect(x_tl, y_tl, x_br - x_tl, y_br - y_tl);
    else
        roi = cv::Rect(0,0,0,0);

    *overlap = roi.width*roi.height;
}

/*!
  @brief Checks if a rectangle is out of bounds of given image, and snips it accordingly.
  @param[in] frameData Image on which rectangle bounds are to be checked.
  @param[in,out] tempRect Rectangle will be modified accordingly.
  */
bool checkBoundary(cv::Mat frameData, cv::Rect& tempRect)
{
    int x2 = tempRect.x + tempRect.width;
    int y2 = tempRect.y + tempRect.height;

    if(tempRect.x < 0)
    {
        if(x2 < 0)
        {
            tempRect = cv::Rect(0,0,0,0);
            return false;
        }
        else
        {
            tempRect.x =0 ;
            tempRect.width = x2;
        }
    }

    if(tempRect.y < 0)
    {
        if(y2 < 0)
        {
            tempRect = cv::Rect(0,0,0,0);
            return false;
        }
        else
        {
            tempRect.y = 0;
            tempRect.height = y2;
        }
    }

    if(tempRect.x > frameData.cols)
    {
        tempRect = cv::Rect(0,0,0,0);
        return false ;
    }

    if(tempRect.y > frameData.rows)
    {
        tempRect = cv::Rect(0,0,0,0);
        return false;
    }

    if(y2 > frameData.rows )
    {
        tempRect.height = frameData.rows - tempRect.y ;
    }

    if(x2 > frameData.cols)
    {
        tempRect.width = frameData.cols - tempRect.x ;
    }

    return true;
}

/*!
  @brief Computes histogram of the given region in an image. Bin size is 32 and considers only first two channels.
  @param[in] frameData Image for which histogram is to be calcualted.
  @param[in] reg        Selected region on image.
  @param[out] hist      Histogram as a row matrix.
  */
void computeColorDistributionRGB(cv::Mat frameData, cv::Rect reg, cv::Mat& hist)
{
    reg.x += cvRound(reg.width*0.2);
    reg.width = cvRound(reg.width*0.6);
    reg.y += cvRound(reg.height*0.1);
    reg.height = cvRound(reg.height*0.8);

    checkBoundary(frameData,reg);
    int bins = 32;

    hist = cv::Mat::zeros(1,pow(bins,2),CV_32FC1);

    // Computing the Right Bottom of the Region
    int rightBotX = ( reg.x ) + ( reg.width );
    int rightBotY = ( reg.y ) + ( reg.height );

    // Computing Color Distribution from the Rectangular/Elliptical Region
    float sumW = 0.0;
    float invBinSize = ( (float) ( bins ) ) / 256.0;
    int binSqr = ( bins ) * ( bins );

    for( int y = ( reg.y ) ; y < rightBotY ; ++y )
    {
        for( int x = ( reg.x ) ; x < rightBotX ; ++x )
        {
            cv::Vec3b color = frameData.at<cv::Vec3b >(y,x);


            // Compute the Bin
            //            int redBin = (floor) (invBinSize * ( (unsigned char) color[2]));
            //            cout << cv::Mat(color) << endl;
            int greenBin = (floor) (invBinSize * ( (unsigned char) color[1]));
            int blueBin = (floor) (invBinSize * ( (unsigned char) color[0]));

            // Compute the Histogram Index
            int histIndx = /*(redBin * binSqr ) +*/ ( greenBin * bins ) + blueBin;

            // Update Histogram and Weight Sum
            hist.at<float>(0,histIndx ) = hist.at<float>(0,histIndx ) + 1;

            sumW = sumW + 1;
        }
    }

    // Normalizing the Histogram
    if( sumW > 0.0001 )
    {
        float inv_sumW = 1.0 / sumW;
        for( int histIndx = 0 ; histIndx < hist.cols  ; ++histIndx )
        {
            hist.at<float>(0, histIndx) = inv_sumW * ( hist.at<float>(0,histIndx ) );
        }
    }
}

void filterOverlappingRois(cv::Mat queryImg, vector< vector<float> >& finalBCs, vector< vector<float> >& finalARerr,
                           vector< vector<float> >& finalScale, vector<int>& foundProductIndex,
                           vector< vector<cv::Rect> >& foundProductRois, vector< vector<int> >& candidateNbrTestIndex)
{
    vector< vector<int> > filterIndex(foundProductRois.size());
    vector< vector<cv::Rect> > filteredRois;
    vector< vector<float> > filteredBCs, filteredARerr, filteredScale;
    vector<int> filteredProductIndex;
    vector< vector<int> > filteredCandidateNbrTestIndex;

    for(int i1=0; i1<foundProductRois.size(); i1++)
        for(int j1=0; j1<foundProductRois[i1].size(); j1++)
            filterIndex[i1].push_back(0);

    for(int i1=0; i1<foundProductRois.size(); i1++)
        for(int j1=0; j1<foundProductRois[i1].size(); j1++)
        {
            for(int l1=0; l1<foundProductRois.size(); l1++)
                for(int m1=0; m1<foundProductRois[l1].size(); m1++)
                {
                    if(l1==i1 && m1==j1)
                        continue;

                    if(filterIndex[i1][j1] == 1)
                        continue;

                    float overlap = 0;
                    cv::Rect r1 = foundProductRois[i1][j1];
                    cv::Rect r2 = foundProductRois[l1][m1];
                    //                checkBoundary(queryImg,r1);
                    //                checkBoundary(queryImg,r2);

                    float scaleFactor1 = 1 - (finalScale[i1][j1] > 1 ? (1 / finalScale[i1][j1]) : finalScale[i1][j1]);
                    float scaleFactor2 = 1 - (finalScale[l1][m1] > 1 ? (1 / finalScale[l1][m1]) : finalScale[l1][m1]);

                    calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);

                    float percentOverlap1 = overlap/(r1.area());
                    float percentOverlap2 = overlap/(r2.area());
                    if(percentOverlap1 > 0.3 || percentOverlap2 > 0.3)
                    {
                        float conf1 = (finalBCs[i1][j1] + (1-abs(1-finalARerr[i1][j1])))/scaleFactor1;
                        float conf2 = (finalBCs[l1][m1] + (1-abs(1-finalARerr[l1][m1])))/scaleFactor2;

                        if(conf1 > conf2)
                            filterIndex[l1][m1] = 1;
                        else
                            filterIndex[i1][j1] = 1;
                    }
                }
        }


    for(int i1=0; i1<foundProductRois.size(); i1++)
    {
        vector<cv::Rect> filteredRoisTemp;
        vector<float> filteredBCsTemp, filteredARerrTemp ,filteredScaleTemp;
        vector<int> filteredCandidateNbrTestIndexTemp;

        for(int j1=0; j1<foundProductRois[i1].size(); j1++)
        {
            if(filterIndex[i1][j1] == 1)
                continue;

            filteredRoisTemp.push_back(foundProductRois[i1][j1]);
            filteredBCsTemp.push_back(finalBCs[i1][j1]);
            filteredARerrTemp.push_back(finalARerr[i1][j1]);
            filteredScaleTemp.push_back(finalScale[i1][j1]);
            filteredCandidateNbrTestIndexTemp.push_back(candidateNbrTestIndex[i1][j1]);
        }

        if(filteredRoisTemp.size() > 0)
        {
            filteredRois.push_back(filteredRoisTemp);
            filteredBCs.push_back(filteredBCsTemp);
            filteredARerr.push_back(filteredARerrTemp);
            filteredScale.push_back(filteredScaleTemp);
            filteredProductIndex.push_back(foundProductIndex[i1]);
            filteredCandidateNbrTestIndex.push_back(filteredCandidateNbrTestIndexTemp);
        }
    }

    foundProductRois = filteredRois;
    finalBCs = filteredBCs;
    finalARerr = filteredARerr;
    finalScale = filteredScale;
    foundProductIndex = filteredProductIndex;
    candidateNbrTestIndex = filteredCandidateNbrTestIndex;
}

cv::Rect findRoifloodFill(cv::Mat img1, vector<cv::Point2f> pts, cv::Rect roiOrg, float& percentFG_FF)
{
    cv::Rect roi = roiOrg;
    checkBoundary(img1,roi);
    cv::Mat fillImg = img1(roi).clone();
    //    cv::cvtColor(fillImg,fillImg,CV_BGR2HSV);

    int diff = 5;
    for(int k=0; k<pts.size(); k++)
    {
        if(!roi.contains(pts[k]))
            continue;
        pts[k] -= cv::Point2f(roi.x,roi.y);

        cv::Rect r1;
        cv::floodFill(fillImg,pts[k],cv::Scalar(0,255,0),&r1,cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));
    }

    cv::Mat growImg = cv::Mat::zeros(fillImg.rows,fillImg.cols,CV_8UC3);
    vector<cv::Point2f> points1;

    for(int i=0; i<fillImg.rows; i++)
        for(int j=0; j<fillImg.cols; j++)
        {
            if(fillImg.at<cv::Vec3b>(i,j) == cv::Vec3b(0,255,0))
            {
                growImg.at<cv::Vec3b>(i,j) = fillImg.at<cv::Vec3b>(i,j);
                points1.push_back(cv::Point2f(j,i));
            }
        }

    cv::dilate(growImg,growImg,cv::Mat());

    percentFG_FF = (float)points1.size()/(float)roi.area();

    cv:: Rect roi2 = cv::Rect(0,0,0,0);

    if(!points1.empty())
    {
        roi2 = cv::boundingRect(points1);

        cv::rectangle(fillImg,roi2,cv::Scalar(0,0,255),2);

        roi2.x += roi.x;
        roi2.y += roi.y;

    }

#if(DEBUG_FF)
    cv::imshow("filled",fillImg);
    cv::waitKey(0);
#endif

    return roi2;

}

/*!
  @brief Finds a point pattern matching using homography based on RANSAC.
  @param[in] templateImg    Source image.
  @param[in] queryImg       Target image.
  @param[in] points1        Source points locations in image.
  @param[in] points2        Target points locations in image.
  @param[out] matchIndex    Matching points indicies wrt input points.
  @param[out] roi           Matched region in target image.
  @return Percentage Matching.
  */

double findPatternMatch(cv::Mat templateImg, cv::Mat queryImg, vector<cv::Point2f> points1, vector<cv::Point2f> points2,
                        vector<int>& matchIndex, cv::Rect& roi,vector<cv::Point2f>& roiPoints,vector<cv::Point2f> &mPointEllipse,
                        cv::Point2f &cPoint,double &scaling, vector<cv::Point2f>& mp2)
{
    // Find homography
    cv::Mat H12 = cv::findHomography(cv::Mat(points1),cv::Mat(points2),CV_RANSAC,RANSAC_THRESHOLD);
    //    cv::Mat H12 = cv::findFundamentalMat(cv::Mat(points1),cv::Mat(points2));
    //    cv::correctMatches(H12,(points1),(points2),(points1),(points2));

    cv::Mat points1t; perspectiveTransform(cv::Mat(points1), points1t, H12);

    int count = 0;
    vector<cv::Point2f> mp1;
    for( size_t i1 = 0; i1 < points1.size(); i1++ )
    {
        if( ( norm(points2[i1] - points1t.at<cv::Point2f>((int)i1,0)) <=10 ))
        {
            count++;
            mp1.push_back(points1[i1]);
            mp2.push_back(points2[i1]);
            matchIndex.push_back(i1);
        }
    }
    if(mp1.size()>=4)
        H12 = cv::findHomography(mp1,(mp2),CV_RANSAC,RANSAC_THRESHOLD);


    //    weightScalingAspect(mp1,mp2,&scaling,30);
    //    cout << "Scaling \t" << scaling << endl;
    //    exit(0);
    mPointEllipse.assign(mp2.begin(),mp2.end());

    double percentage_match = (double)(count*100)/((float)points1.size());

    // Transform region points
    vector<cv::Point2f> centerVec;
    cv::Point2f centerSURF(templateImg.cols/2,templateImg.rows/2);
    cv::Mat centerVecOut;
    centerVec.push_back(centerSURF);
    centerVec.push_back(cv::Point2f(0,0));

#if(POINT_RECT)
    centerVec.push_back(cv::Point2f(templateImg.cols,0));
#endif

    centerVec.push_back(cv::Point2f(templateImg.cols,templateImg.rows));

#if(POINT_RECT)
    centerVec.push_back(cv::Point2f(0,templateImg.rows));
#endif

    perspectiveTransform((cv::Mat)centerVec, centerVecOut, H12);
    centerSURF = centerVecOut.at<cv::Point2f>(0);

    cPoint = centerSURF;


#if(POINT_RECT)
    for(int k1=1; k1<centerVecOut.rows; k1++)
        roiPoints.push_back(centerVecOut.at<cv::Point2f>(k1));
#else

    roi.x = centerVecOut.at<cv::Point2f>(1).x;
    roi.y = centerVecOut.at<cv::Point2f>(1).y;
    roi.width = abs(centerVecOut.at<cv::Point2f>(2).x - centerVecOut.at<cv::Point2f>(1).x);
    roi.height = abs(centerVecOut.at<cv::Point2f>(2).y - centerVecOut.at<cv::Point2f>(1).y);

#endif

    //    cPoint = cv::Point2f(roi.x+roi.width/2,roi.y+roi.height/2);

    roi.x = cPoint.x - roi.width/2;
    roi.y = cPoint.y - roi.height/2;

    // Generate display data
    int numRows = queryImg.rows > templateImg.rows ? queryImg.rows : templateImg.rows;
    cv::Mat drawImg(numRows,queryImg.cols+templateImg.cols,CV_8UC3,cv::Scalar::all(0));
    templateImg.copyTo(drawImg.colRange(0,templateImg.cols).rowRange(0,templateImg.rows));
    queryImg.copyTo(drawImg.colRange(templateImg.cols,drawImg.cols).rowRange(0,queryImg.rows));

    for(int i=0; i<mp1.size(); i++)
    {
        cv::Point2f pt = mp2[i] + cv::Point2f(templateImg.cols,0);
        cv::line(drawImg,mp1[i],pt,cv::Scalar(0,255,0),2,CV_AA);
    }
    cv::Point2f pt = centerSURF + cv::Point2f(templateImg.cols,0);
    cv::circle(drawImg,pt,2,cv::Scalar(255,0,0),2);

    cv::Rect roi2 = roi;
    roi2.x += templateImg.cols;
    cv::rectangle(drawImg,roi2,cv::Scalar(0,255,255),2);

#if(DEBUG_PM)
    imshow("drawimgPM",drawImg);
    cv::waitKey(0);
#endif
    return percentage_match;
}


void connectedComponent(cv::Mat input, cv::Mat mask, cv::Point2f seed, cv::Vec3b color, float diff)
{
    cv::Rect imgBound(0,0,input.cols,input.rows);

    if(!imgBound.contains(seed))
        return;

    bool fg = true;//pointRecognise(input,seed,emModelFG,emModelBG);

    vector<cv::Point2f> queue;
    vector<cv::Point2f> sign;
    sign.push_back(cv::Point2f(-1,-1));
    sign.push_back(cv::Point2f(0,-1));
    sign.push_back(cv::Point2f(1,-1));
    sign.push_back(cv::Point2f(-1,0));
    sign.push_back(cv::Point2f(1,0));
    sign.push_back(cv::Point2f(-1,1));
    sign.push_back(cv::Point2f(0,1));
    sign.push_back(cv::Point2f(1,1));

    if(fg)
    {
        input.at<cv::Vec3b>(seed.y,seed.x) = color;
        queue.push_back(seed);

        while(!queue.empty())
        {
            cv::Point2f pt1 = queue[queue.size()-1];
            cv::Vec3b val = input.at<cv::Vec3b>(pt1.y,pt1.x);

            queue.pop_back();

            for(int i=0; i<sign.size(); i++)
            {
                cv::Point2f pt1Nbr = pt1 + sign[i];

                if(!imgBound.contains(pt1Nbr))
                    continue;

                cv::Vec3b nbrVal = input.at<cv::Vec3b>(pt1Nbr.y,pt1Nbr.x);

                if(mask.at<cv::Vec3b>(pt1Nbr.y,pt1Nbr.x) != cv::Vec3b(0,0,0) || nbrVal == color)
                    continue;

                float distNbr = norm(val-nbrVal);
                bool fgNbr = false;//pointRecognise(input,pt1Nbr,emModelFG,emModelBG);

                if(distNbr < diff)
                    fgNbr = true;

                if(fgNbr)
                {
                    input.at<cv::Vec3b>(pt1Nbr.y,pt1Nbr.x) = color;
                    queue.push_back(pt1Nbr);
                }
                else
                {
                    continue;
                }
            }
        }
    }
    else
    {
        return;
    }
}


/*!
  @brief Finds a random color for a given seed, defualt seed id current timestamp in nanoseconds.
  @param[in]    Input seed for random number generator.
  @return       Random Scalar color value.
  */
cv::Scalar getRandColor(/*uint64_t seed = ros::Time::now().toNSec()*/)
{
    timespec t1;
    clock_gettime(0,&t1);
    cv::RNG randNum(t1.tv_nsec);
    //    RNG randNum(seed);
    cv::Scalar randColor;

    int t = randNum.uniform(0,256);
    for(int k=0; k<3; k++)
    {
        t = randNum.uniform(0,256);
        randColor[k] = t;
    }

    return randColor;
}

/*!
  @brief Finds the delaunay triangulation and then Euclidean Minimum Spanning Tree using Prim's algorithm. Marks the graph on image.
  @param[in]    img         Image to draw the graph.
  @param[in]    points1     Input points on which the graph/tree has to made.
  @param[out]   adjMat      Adjacency Matrix for the EMST. Value is the index of the edge. -1 if no edge exists for the pair.
  @param[out]   nbrList     List of neighbours for the given points in form of a vector. It needs to be inititated with points size.
  */
void draw_subdiv(cv::Mat img, vector<cv::Point2f> points1, cv::Mat_<int>& adjMat, vector< vector<int> >& nbrList )
{
    adjMat = cv::Mat_<int>(points1.size(),points1.size(),-1);

    for(int k2=0; k2<points1.size(); k2++)
    {
        char txt[20];
        sprintf(txt,"%d",k2);
        cv::putText(img,txt,points1[k2],1,1,cv::Scalar(0,255,0),2);
    }
    cv::Rect rect = cv::Rect(0,0,800,800);
    cv::Subdiv2D subdiv = cv::Subdiv2D(rect);

    subdiv.insert(points1);

#if 0
    vector<Vec6f> triangleList;
    subdiv.getTriangleList(triangleList);
    vector<Point> pt(3);

    for( size_t i = 0; i < triangleList.size(); i++ )
    {
        Vec6f t = triangleList[i];
        pt[0] = Point(cvRound(t[0]), cvRound(t[1]));
        pt[1] = Point(cvRound(t[2]), cvRound(t[3]));
        pt[2] = Point(cvRound(t[4]), cvRound(t[5]));
        line(img, pt[0], pt[1], delaunay_color, 1, CV_AA, 0);
        line(img, pt[1], pt[2], delaunay_color, 1, CV_AA, 0);
        line(img, pt[2], pt[0], delaunay_color, 1, CV_AA, 0);
    }
#else
    vector<cv::Vec4f> edgeList;
    vector<float> edgeLength;
    vector<cv::Point2f> edgePoints;
    subdiv.getEdgeList(edgeList);
    for( size_t i = 0; i < edgeList.size(); i++ )
    {
        cv::Vec4f e = edgeList[i];
        cv::Point2f pt0(e[0],e[1]); //= cv::Point(cvRound(e[0]), cvRound(e[1]));
        cv::Point2f pt1(e[2],e[3]); //= cv::Point(cvRound(e[2]), cvRound(e[3]));
        if(e[0] < 0 || e[0] > 640 || e[1] < 0 || e[1] > 480 || e[2] < 0 || e[2] > 640 || e[3] < 0 || e[3] > 480)
            continue;
        //        if(norm(pt0-pt1) < 50)
        edgeLength.push_back(norm(pt0-pt1));
        edgePoints.push_back(cv::Point2f(e[0],e[1]));
        edgePoints.push_back(cv::Point2f(e[2],e[3]));


        cv::line(img, pt0, pt1, cv::Scalar(0,0,255), 2, CV_AA, 0);

        int eg0,eg1,vx0,vx1;
        subdiv.locate(pt0,eg0,vx0);
        subdiv.locate(pt1,eg1,vx1);

        adjMat(vx0-4, vx1-4) = 1;
        adjMat(vx1-4, vx0-4) = 1;

//        nbrList[vx0-4].push_back(vx1-4);
//        nbrList[vx1-4].push_back(vx0-4);
    }
#endif

    vector<int> sortedEdgeIdx;
    cv::sortIdx(edgeLength,sortedEdgeIdx,CV_SORT_ASCENDING);

    //**********************************************************************************************************
    // Prim's Algorithm for EMST
    vector< vector< int > > vertexEdges(points1.size());
    vector< vector< float > > vertexEdgeLength(points1.size());

    // Store all the Delaunay neoghbors in a sorted order along with the corresponding edgeLengths
    for(uint i=0; i<edgeLength.size(); i++)
    {
        int vtx1, vtx2, eg1, eg2;
        subdiv.locate(edgePoints[2*sortedEdgeIdx[i]],eg1,vtx1);
        subdiv.locate(edgePoints[2*sortedEdgeIdx[i]+1],eg2,vtx2);

        vertexEdges[vtx1-4].push_back(vtx2-4);
        vertexEdges[vtx2-4].push_back(vtx1-4);

        vertexEdgeLength[vtx1-4].push_back(edgeLength[sortedEdgeIdx[i]]);
        vertexEdgeLength[vtx2-4].push_back(edgeLength[sortedEdgeIdx[i]]);
    }

    // Stores the vertices that have been parsed
    // and search for a minimum weight edge is done in the neighbors of these only
    vector<int> vertexCounter;
    vertexCounter.push_back(0); // Choose a vertex arbitrarily


    // Mark the vertices that have been parsed
    vector<int> vertexDone(points1.size(),0);
    vertexDone[0] = 1;

    int counter = 0;
    while(vertexCounter.size() < points1.size())
    {
        float minVal = 9999;
        cv::Point2i minValIdx;

        // Loop over all the traversed vertices
        for(uint j=0; j<vertexCounter.size(); j++)
        {
            // Loop over all the neighbors of the traversed vertex
            for(uint k=0; k<vertexEdges[vertexCounter[j]].size(); k++)
            {
                if(!vertexDone[vertexEdges[vertexCounter[j]][k]])
                {
                    if(vertexEdgeLength[vertexCounter[j]][k] < minVal)
                    {
                        minVal = vertexEdgeLength[vertexCounter[j]][k];
                        minValIdx = cv::Point(vertexEdges[vertexCounter[j]][k], vertexCounter[j]);
                    }
                    break;
                }
            }
        }

        vertexCounter.push_back(minValIdx.x);
        vertexDone[minValIdx.x] = 1;

        //        line(img,points1[minValIdx.x], points1[minValIdx.y], Scalar(0,0,255), 2, CV_AA);
//        adjMat(minValIdx.y, minValIdx.x) = counter;
//        adjMat(minValIdx.x, minValIdx.y) = counter;

//        nbrList[minValIdx.y].push_back(minValIdx.x);
//        nbrList[minValIdx.x].push_back(minValIdx.y);

        counter ++;

    }
    //**********************************************************************************************************
}

void floodFill_(cv::Mat img1,vector<cv::Point2f>& clusterCenters, vector<cv::Vec3b>& colors,
                vector<cv::Rect>& patchRois, cv::Mat& outImg)
{
    clusterCenters.clear();
    colors.clear();
    patchRois.clear();

    cv::Mat queryImg = img1.clone();

    cv::medianBlur(queryImg,queryImg,5);

    cv::Mat fillImg=queryImg.clone();

    cv::cvtColor(queryImg,fillImg,CV_BGR2HSV);

    cv::Mat imgColor = cv::Mat::zeros(fillImg.rows,fillImg.cols,CV_8UC3);;

    cv::Mat maskImg = cv::Mat::zeros(fillImg.rows+2,fillImg.cols+2,CV_8UC1);
    int diff = 5;

    cv::Rect reg(0,0,fillImg.cols,fillImg.rows);

    for(int j=0; j<fillImg.cols; j+=5)
    {
        for(int i=0; i<fillImg.rows; i+=5)
        {
            fillImg = queryImg.clone();
            cv::Rect r1;

            int area = cv::floodFill(fillImg,maskImg,cv::Point2f(j,i),cv::Scalar(0,255,0),&r1,cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));

            //            cv::Scalar tempColor = getRandColor();

            //            connectedComponent(fillImg,maskImg,cv::Point2f(j,i),tempColor,cv::Scalar(0,255,0));

            cv::Vec3b color1 = queryImg.at<cv::Vec3b>(i,j);//getRandColor();

            cv::Mat roiImg = queryImg(r1).clone();
            cv::Scalar color2 = cv::mean(roiImg);
            color1 = cv::Vec3b(color2[0],color2[1],color2[2]);


            if(area < 0.01*reg.area())
                continue;

            cv::Point2f center;
            center.x = r1.x + r1.width/2;
            center.y = r1.y + r1.height/2;

            clusterCenters.push_back(center);
            colors.push_back(color1);
            patchRois.push_back(r1);

            for(int j=0; j<fillImg.cols; j++)
            {
                for(int i=0; i<fillImg.rows; i++)
                {
                    if(fillImg.at<cv::Vec3b>(i,j) == cv::Vec3b(0,255,0))
                        imgColor.at<cv::Vec3b>(i,j) = color1;
                }
            }

        }
    }

    cv::medianBlur(imgColor,imgColor,5);

    //    cv::imshow("filled",imgColor);
    //    cv::waitKey(0);

    outImg = imgColor.clone();

}


/*!
  @brief Image segmentation based on K-Means using color as well as pixel position.
  @param[in]    segmentImg  Image tp be segmented
  @param[in]    K           Number of clusters to be formed
  @param[out]   imgLabels   Labels correspoding to pixels of the image.
  @param[out]   imgMeans    Cluster Centers.
  @param[out]   outImg      Output clustered image with each region marked as mean color value.
  */
void clusterImgWithPos(cv::Mat input, int K, vector<cv::Point2f>& clusterCenters, vector<cv::Vec3b>& colors,
                       cv::Mat& imgMeans, cv::Mat& outImg)
{
    cv::Mat segmentImg = input.clone();
    cv::Rect roi1(0,0,segmentImg.cols,segmentImg.rows);

    cv::medianBlur(segmentImg,segmentImg,5);

    cv::Mat samples;
    cv::Mat imgColor = segmentImg.clone();

    cv::cvtColor(segmentImg,segmentImg,CV_BGR2HSV);

    for(int j=0; j<segmentImg.cols; j++)
    {
        for(int k=0; k<segmentImg.rows; k++)
        {
            cv::Mat temp(1,5,CV_64FC1);
            temp.at<double>(0,0) = 100*double(segmentImg.at<cv::Vec3b>(k,j)[0])/180.0;
            temp.at<double>(0,1) = 100*double(segmentImg.at<cv::Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(segmentImg.at<cv::Vec3b>(k,j)[2])/255.0;
            temp.at<double>(0,3) = 100*double(j)/roi1.width;
            temp.at<double>(0,4) = 100*double(k)/roi1.height;

            samples.push_back(temp.row(0));
        }
    }

    samples.convertTo(samples,CV_32FC1);
    cv::Mat imglabels;
    cv::kmeans(samples,K,imglabels,cv::TermCriteria( CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 300, 0.1 ), 1,cv::KMEANS_RANDOM_CENTERS,imgMeans);

    //    vector< vector<Mat> > clDa;
    //    adaptiveKmeans(samples,75,imgMeans,imglabels,clDa);

    vector<cv::Mat> clusteredImages(K);

    for(int k=0; k<imgMeans.rows; k++)
    {
        cv::Vec3b color1;
        color1[0] = uchar(2.55 * imgMeans.at<float>(k,0));
        color1[1] = uchar(2.55 * imgMeans.at<float>(k,1));
        color1[2] = uchar(2.55 * imgMeans.at<float>(k,2));

        colors.push_back(color1);

        clusteredImages[k] = cv::Mat::zeros(segmentImg.rows,segmentImg.cols,CV_8UC3);
    }

    int counter = 0;
    for(int j=0; j<imgColor.cols; j++)
    {
        for(int i=0; i<imgColor.rows; i++)
        {
            int label = imglabels.at<int>(counter);
            imgColor.at<cv::Vec3b>(i,j) = colors[label];
            counter++;
            clusteredImages[label].at<cv::Vec3b>(i,j) = colors[label];
        }
    }

    for(int k=0; k<imgMeans.rows; k++)
    {
        cv::Point2f pt1;
        pt1.x = roi1.width * imgMeans.at<float>(k,3) / 100;
        pt1.y = roi1.height * imgMeans.at<float>(k,4) / 100;
        char text[100];
        sprintf(text,"%d",k);
        //        putText(imgColor,text,pt1,1,1,Scalar(0,0,0),2);

        clusterCenters.push_back(pt1);
    }

    cv::Mat_<int> adjMat(clusterCenters.size(), clusterCenters.size(), -1);
    vector< vector<int> > nbrList(clusterCenters.size());
    draw_subdiv(imgColor,clusterCenters,adjMat,nbrList);

    // Commented part below is for background subtraction

    //    int diff = 5;

    //    Mat imgColor3 = imgColor.clone();

    //    Rect reg(0,0,segmentImg.cols,segmentImg.rows);
    //    reg.x += cvRound(reg.width*0.2);
    //    reg.width = cvRound(reg.width*0.6);
    //    reg.y += cvRound(reg.height*0.1);
    //    reg.height = cvRound(reg.height*0.8);

    //    vector<Point2f> polyPoints;
    //    polyPoints.push_back(Point2f(reg.x,reg.y));
    //    polyPoints.push_back(Point2f(reg.x+reg.width,reg.y));
    //    polyPoints.push_back(Point2f(reg.x+reg.width,reg.y+reg.height));
    //    polyPoints.push_back(Point2f(reg.x,reg.y+reg.height));

    //    for(int k=0; k<clusteredImages.size(); k++)
    //    {
    ////        imshow("11",clusteredImages[k]);
    //        for(int j=0; j<imgColor3.cols; j++)
    //        {
    //            for(int i=0; i<imgColor3.rows; i++)
    //            {
    //                Point2f pt1(j,i);
    //                //            if(reg.contains(pt1) != 0)
    //                //                continue;
    //                if(clusteredImages[k].at<Vec3b>(i,j) == Vec3b(0,0,0))
    //                    continue;
    //                if(pointPolygonTest(polyPoints,pt1,false) == 1)
    //                    continue;
    //                else
    //                {
    ////                    waitKey(0);
    //                    cv::Rect r1;
    //                    cv::floodFill(imgColor,pt1,cv::Scalar(0,0,0),&r1,cv::Scalar(diff,diff,diff),cv::Scalar(diff,diff,diff));
    //                }
    //            }
    //        }
    ////        imshow("12",clusteredImages[k]);
    ////        waitKey(0);
    //    }


    cv::medianBlur(imgColor,imgColor,3);

    //    cv::imshow("imgShowColor",imgColor);
    //    cv::waitKey(0);

    outImg = imgColor.clone();
}


void findKNN(cv::Mat imgMeansSrc, cv::Mat imgMeansTarget, int KNN, vector< vector<int> >& nbrIndex,
             vector< vector<float> >& nbrDis)
{
    for(int i=0; i<imgMeansSrc.rows; i++)
    {
        vector<float> distances;

        for(int j=0; j<imgMeansTarget.rows; j++)
        {
            float dist = norm(imgMeansSrc.row(i) - imgMeansTarget.row(j));
            distances.push_back(dist);
        }

        vector<int> sortedIndex;
        vector<int> temp;
        if(!distances.empty())
        {
            cv::sortIdx(distances,sortedIndex,CV_SORT_EVERY_ROW || CV_SORT_ASCENDING);
            temp.assign(sortedIndex.begin(),sortedIndex.begin()+(KNN>distances.size()?distances.size():KNN));
        }

//        cout << cv::Mat(distances) << "\t" << cv::Mat(temp) << endl;
        nbrIndex.push_back(temp);
        nbrDis.push_back(distances);
    }
}


void findTargetCandidates(cv::Mat input, cv::Mat imgMeansSrc, int K, vector< vector<cv::Point2f> >& targetCandidates)
{
    cv::Mat segmentImg = input.clone();
    cv::Rect roi1(0,0,segmentImg.cols,segmentImg.rows);

    cv::medianBlur(segmentImg,segmentImg,5);

    cv::Mat samples;
    cv::Mat imgColor = segmentImg.clone();

    cv::cvtColor(segmentImg,segmentImg,CV_BGR2HSV);

    for(int j=0; j<segmentImg.cols; j++)
    {
        for(int k=0; k<segmentImg.rows; k++)
        {
            cv::Mat temp(1,5,CV_64FC1);
            temp.at<double>(0,0) = 100*double(segmentImg.at<cv::Vec3b>(k,j)[0])/180.0;
            temp.at<double>(0,1) = 100*double(segmentImg.at<cv::Vec3b>(k,j)[1])/255.0;
            temp.at<double>(0,2) = 100*double(segmentImg.at<cv::Vec3b>(k,j)[2])/255.0;
            temp.at<double>(0,3) = 100*double(j)/roi1.width;
            temp.at<double>(0,4) = 100*double(k)/roi1.height;

            samples.push_back(temp.row(0));
        }
    }

    samples.convertTo(samples,CV_32FC1);
    cv::Mat imglabels, imgMeansTarget;
    cv::kmeans(samples,K,imglabels,cv::TermCriteria( CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 300, 0.1 ), 1,cv::KMEANS_RANDOM_CENTERS,imgMeansTarget);

    vector<cv::Vec3b> colors(imgMeansSrc.rows);

    for(int k=0; k<imgMeansSrc.rows; k++)
    {
        colors[k][0] = uchar(2.55 * imgMeansSrc.at<float>(k,0));
        colors[k][1] = uchar(2.55 * imgMeansSrc.at<float>(k,1));
        colors[k][2] = uchar(2.55 * imgMeansSrc.at<float>(k,2));
    }

    int counter = 0;
    for(int j=0; j<imgColor.cols; j++)
    {
        for(int i=0; i<imgColor.rows; i++)
        {
            int label = imglabels.at<int>(counter);
            imgColor.at<cv::Vec3b>(i,j) = colors[label];
            counter++;
        }
    }


    vector< vector<int> > nbrIndex;
    vector< vector<float> > nbrDists;
    findKNN(imgMeansSrc,imgMeansTarget,2,nbrIndex,nbrDists);

    for(int i=0; i<nbrIndex.size(); i++)
    {
        vector<cv::Point2f> tempPoints;
        for(int j=0; j<nbrIndex[i].size(); j++)
        {
            int index = nbrIndex[i][j];

            cv::Point2f pt1;
            pt1.x = roi1.width * imgMeansTarget.at<float>(0,3) / 100;
            pt1.y = roi1.height * imgMeansTarget.at<float>(0,4) / 100;

            tempPoints.push_back(pt1);
        }

        targetCandidates.push_back(tempPoints);
    }


    vector<cv::Point2f> clusterCenters;
    for(int k=0; k<imgMeansTarget.rows; k++)
    {
        cv::Point2f pt1;
        pt1.x = roi1.width * imgMeansTarget.at<float>(k,3) / 100;
        pt1.y = roi1.height * imgMeansTarget.at<float>(k,4) / 100;

        clusterCenters.push_back(pt1);
    }

    cv::Mat_<int> adjMat(clusterCenters.size(), clusterCenters.size(), -1);
    vector< vector<int> > nbrList(clusterCenters.size());
    draw_subdiv(imgColor,clusterCenters,adjMat,nbrList);

    //    cv::imshow("targetRaw",segmentImg);
    cv::imshow("target2",imgColor);
    cv::waitKey(0);

}


// Function for finding the factors for a given adjacenecy matrix of a graph
// Parameters:
// factorFile -     Input/Output, stores the factors in particular format
// adjMat -         Input adjacency matrix of the graph
// candidates -     Input candidates for the nodes of graph
// edgePotentials - Output, stores edge potentials for each possible edge, in order to calculate the error of the selected edge
void getFactors(ofstream& factorFile, cv::Mat_<int> adjMat, vector<cv::Point2f> points1, vector< vector<float> > nbrDists,
                vector< vector<cv::Point2f> > candidates, vector< cv::Mat_<float> >& edgePotentials)
{
    // Iterate over the adjacency matrix (upper triangualar part only)
    for(uint i=0; i<adjMat.rows; i++)
        for(uint j=i+1; j<adjMat.cols; j++)
        {
//            cout << adjMat[i][j] << endl;
            // only if there is a connection between two nodes
            if(adjMat[i][j]!=-1)
            {
                cv::Mat_<float> truthTable(candidates[i].size(),candidates[j].size(),0.0);
                //                // for this particular case, the required angle between nodes is -90
                //                float theta1 = -90;

                // Find the angle between source nodes
                //                float theta1 = float(180 * 7.0/22.0) *
                //                        ( atan2( (points1[i].y - points1[j].y), (points1[i].x - points1[j].x) ) );

                //                if(isnan(theta1))
                //                    theta1 = -90;

                float r1 = norm(points1[i]-points1[j]);

                factorFile << 2 << endl; // Number of variables in the factor
                factorFile << i << " " << j << endl; // Indicies of those variables
                factorFile << candidates[i].size() << " " << candidates[j].size() << endl; // Cardinality of each variable
                factorFile <<  candidates[i].size() * candidates[j].size() << endl; // Non Zero states

                int factorCounter=0;

                // Iterate over all the possible candidates for given pair of nodes
                for(int m=0; m<candidates[j].size(); m++)
                {
                    //                    candidateError[j][m] = 0;
                    for(int l=0; l<candidates[i].size(); l++)
                    {
                        //                        candidateError[i][l] = 0;
                        {
                            // Find the angle between candidate nodes
                            float theta2 = float(180 * 7.0/22.0) *
                                    ( atan2( (candidates[i][l].y - candidates[j][m].y), (candidates[i][l].x - candidates[j][m].x) ) );

                            if(isnan(theta2))
                                theta2 = -90;

                            float r2 = norm(candidates[i][l] - candidates[j][m]);

                            // Calcualte edge potential
                            //                            float psi = exp(- 5*abs(theta1-theta2)/100 );
                            float psi = exp(- 5*abs(r1-r2)/100 - (nbrDists[i][l])/700 - (nbrDists[j][m])/700);

                            float psi_old = psi;
                            // if it is too less, punish further, possibly an outlier
                            if(psi < 0.3)
                            {
                                psi = 0.0001;
                            }

                            factorFile << factorCounter << " " << psi << endl; // factor state index and value

                            truthTable(l,m) = psi;

//                            cout << i << "\t" << j << "\t" << l << "\t" << m << "\t" <<
//                                    theta2 << "\t" << psi_old << "\t" << psi << "\t" << abs(r1-r2) << "\t" << nbrDists[i][l]
//                                 << "\t" << nbrDists[j][m] << endl;

                            factorCounter++;
                        }
                    }
                }
                edgePotentials.push_back(truthTable);

                factorFile << "\n" << endl;
            }
        }
}

// Finds graph matching based on MAP solution using BP for a given MRF model of an underlying graph
// Parameters:
// adjMat -     Input adjacency matrix of the graph (n x n, n is number of nodes of graph)
// candidates - Input possible candidates for each node of graph
// outpoints -  Output selected candidates
// partsFound - Output flags vector for parts found or not found
// matchIndex - Output indicies of selected candidates
void findMatch_MRF_MAP(cv::Mat_<int> adjMat, vector<cv::Point2f> points1, vector< vector<float> > nbrDists,
                       vector< vector<cv::Point2f> > candidates, vector<cv::Point2f>& outPoints,
                       vector<int>& partsFound, vector<int>& matchIndex)
{
    // File to store the factors in a particular format
    ofstream factorFile;
    factorFile.open("factor.fg");

    //    factorFile << adjMat.rows-1 << "\n" << endl;

//        cout << adjMat << endl;
    int numEdges = 0;
    for(int i1=0; i1<adjMat.rows; i1++)
        for(int j1=i1+1; j1<adjMat.cols; j1++)
            if(adjMat[i1][j1]!=-1)
                numEdges++;

    factorFile << numEdges << "\n" << endl;

    vector< cv::Mat_<float> > edgePotentials;

    // Function for calculating factors
    getFactors(factorFile,adjMat,points1,nbrDists,candidates,edgePotentials);

    // Reading factors from the file (written above)
    dai::FactorGraph fg;
    fg.ReadFromFile("factor.fg");

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 1;

    // Store the constants in a PropertySet object
    dai::PropertySet opts;
    opts.set("maxiter",maxiter);  // Maximum number of iterations
    opts.set("tol",tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)

    // Construct a BP (belief propagation) object from the FactorGraph fg
    // using the parameters specified by opts and two additional properties,
    // specifying the type of updates the BP algorithm should perform and
    // whether they should be done in the real or in the logdomain
    //
    // Note that inference is set to MAXPROD, which means that the object
    // will perform the max-product algorithm instead of the sum-product algorithm
    dai::BP mp(fg, opts("updates",string("SEQMAX"))("logdomain",false)("inference",string("MAXPROD"))("damping",string("0.1")));
    // Initialize max-product algorithm
    mp.init();
    // Run max-product algorithm
    mp.run();
    // Calculate joint state of all variables that has maximum probability
    // based on the max-product result
    vector<size_t> mpstate = mp.findMaximum();

    for( size_t i = 0; i < mpstate.size(); i++ )
    {
        // Store the output points selected by MAP solution (mpstate stores the selected candidate index)
        outPoints.push_back(candidates[i][mpstate[i]]);
        matchIndex.push_back(mpstate[i]);
    }

    // Check if selected edgePotential is in range
    int edgeCounter=0;
    for(uint i=0; i<adjMat.rows; i++)
        for(uint j=i+1; j<adjMat.cols; j++)
        {
            // only if there is a connection between two nodes
            if(adjMat[i][j]!=-1)
            {
                if(edgePotentials[edgeCounter](mpstate[i],mpstate[j]) < 0.1)
                {
                    partsFound.push_back(-1);
                }
                else
                {
                    partsFound.push_back(1);
                }

                edgeCounter++;
            }
        }
}



void findNeighbourProducts(cv::Mat queryImg, vector<cv::Mat> templateHistograms, vector<string> imgFileNames,
                           vector<cv::KeyPoint> queryKeypoint,cv::Mat queryDesc,
                           vector< vector<cv::KeyPoint> > templateKeyPoints, vector<cv::Mat> templateDesc,
                           vector<cv::EM> emModelTemplate, cv::EM emModelBG, vector<int> pointTemplateIndex,
                           vector<int> foundProductIndex,  vector< vector<cv::Rect> >& foundProductRois,
                           vector< vector<float> >& finalBCs, vector< vector<float> >& finalARerr, vector< vector<float> >& finalScale,
                           vector< vector<int> >& candidateNbrTestIndex)
{
    cv::Mat queryImgHSV;
    cv::cvtColor(queryImg,queryImgHSV,CV_BGR2HSV);
    // Find neighbor items of similar type

    // Sign decides the different neighbouring areas around the identified product
    vector<cv::Point2f> sign;
    sign.push_back(cv::Point2f(1,0));
    sign.push_back(cv::Point2f(0,1));
    sign.push_back(cv::Point2f(-1,0));
    sign.push_back(cv::Point2f(0,-1));
    sign.push_back(cv::Point2f(1,1));
    sign.push_back(cv::Point2f(-1,-1));
    sign.push_back(cv::Point2f(-1,1));
    sign.push_back(cv::Point2f(1,-1));

    int numProducts = foundProductRois.size();
    vector<cv::Rect> candidateRois;
    vector< vector<cv::Rect> > candidateNbrRois(numProducts);
    for(int i1=0; i1<foundProductRois.size(); i1++)
    {
        for(int k1=0; k1<foundProductRois[i1].size(); k1++)
        {
            if(candidateNbrTestIndex[i1][k1] == 1)
                candidateNbrTestIndex[i1][k1] = 0;
            else
                continue;

            cv::Rect r1 = foundProductRois[i1][k1];

            for(int j1=0; j1<sign.size(); j1++)
            {
                cv::Rect r2 = r1;

                r2.x += ( sign[j1].x * r1.width);
                r2.y += ( sign[j1].y * r1.height);

                bool roiInsideImage = checkBoundary(queryImg,r2);
                float percentVisibleROI = 100.0 * (float)r2.area() / (float)r1.area();
                if(!roiInsideImage || percentVisibleROI < 20)
                    continue;

                bool objectOverlap = false;
                for(int l1=0; l1<foundProductRois.size(); l1++)
                {
                    for(int m1=0; m1<foundProductRois[l1].size(); m1++)
                    {
                        // Need not find overlap with the centre product itself
                        if(l1 == k1 && m1 == j1)
                            continue;

                        cv::Rect r3 = foundProductRois[l1][m1];

                        float overlap=0;
                        calcOverlap(r2.tl(),r3.tl(),r2.size(),r3.size(),&overlap);

                        float percentOverlap = float(overlap)/float(r2.area());

                        if(percentOverlap > 0.2)
                        {
                            objectOverlap= true;
                            break;
                        }
                    }
                }

                if(!objectOverlap)
                {
                    candidateRois.push_back(r2);
                    candidateNbrRois[i1].push_back(r2);
                }
            }
        }
    }



    // Filter out the candidate ROIs based on histogram matching

    vector< vector<cv::Rect> > filteredCandidateRois(numProducts);

#if(GMM_DECIDE_ROI)
    vector< vector<cv::Rect> > filteredCandidateGMMroi(numProducts);
    vector< vector<float> > filteredCandidatePercentFG(numProducts);
#endif

    for(int j1=0; j1<candidateNbrRois.size(); j1++)
    {
        cv::Mat templateHist = templateHistograms[foundProductIndex[j1]];

        for(int i1=0; i1<candidateNbrRois[j1].size(); i1++)
        {
            bool productFound = false;


#if(PRODUCT_RECO_GMM)
            cv::Mat outImg;
            float percentFG=0;
            cv::Rect roiTemp = candidateNbrRois[j1][i1], boundRect;
            checkBoundary(queryImg,roiTemp);
            if(roiTemp.area() != 0)
            {
                productFound = productRecognise(queryImg(roiTemp).clone(),emModelTemplate[foundProductIndex[j1]],
                                                emModelBG,percentFG,outImg,boundRect);
                boundRect.x += roiTemp.x;
                boundRect.y += roiTemp.y;
            }
#else

            cv::Mat roiHist;
            computeColorDistributionRGB(queryImgHSV,candidateNbrRois[j1][i1],roiHist);

            double bhattCoeff = 0;

            for(int k1=0; k1<templateHist.cols; k1++)
            {
                bhattCoeff += sqrt(templateHist.at<float>(0,k1)*roiHist.at<float>(0,k1));
            }

            if(bhattCoeff > 0.5)
                productFound = true;
#endif
            if(productFound)
            {
                filteredCandidateRois[j1].push_back(candidateNbrRois[j1][i1]);
#if(GMM_DECIDE_ROI)
                filteredCandidateGMMroi[j1].push_back(boundRect);
                filteredCandidatePercentFG[j1].push_back(percentFG);
#endif


#if(DEBUG_NBR)

#if(PRODUCT_RECO_GMM)
                cv::imshow("prodReco",outImg);
#else
                cv::Mat imgTemp = queryImg.clone();
                cv::rectangle(imgTemp,candidateNbrRois[j1][i1],cv::Scalar(0,255,255),2);
                cv::imshow("histRoi",imgTemp);
                cv::waitKey(0);
#endif

#endif
            }

        }
    }



    // Do Surf Matching for getting the remaining candidates

    for(int k1=0; k1<filteredCandidateRois.size(); k1++)
    {
        float validScaleVal = finalScale[k1][0];

        for(int i1=0; i1<filteredCandidateRois[k1].size(); i1++)
        {
            int itemIndex = foundProductIndex[k1];

//            cout << k1 << "\t" << i1 << "\t" << itemIndex << endl;

            cv::Rect testRoi = filteredCandidateRois[k1][i1];

#if(GMM_DECIDE_ROI)
            cv::Rect gmmRoi = filteredCandidateGMMroi[k1][i1];
#endif

            cv::Mat templateImg = cv::imread(imgFileNames[itemIndex]);

            vector<cv::KeyPoint> kp1(templateKeyPoints[itemIndex].size());
            kp1.assign(templateKeyPoints[itemIndex].begin(),templateKeyPoints[itemIndex].end());

            cv::Mat desc1 = templateDesc[itemIndex].clone();

            checkBoundary(queryImg,testRoi);

            vector<cv::KeyPoint> kp2, kp2MatchedTemplateOnly; // Set of KP inside testROI and a subset which contains only matched pts
            cv::Mat desc2;
            for(int j1=0; j1<queryKeypoint.size(); j1++)
            {
                if(testRoi.contains(queryKeypoint[j1].pt))
                {
                    kp2.push_back(queryKeypoint[j1]);
                    desc2.push_back(queryDesc.row(j1));

                    if(pointTemplateIndex[j1] == itemIndex)
                        kp2MatchedTemplateOnly.push_back(queryKeypoint[j1]);
                }
            }

            bool surfResult = false, floodFillResult = false;
            bool scaleValidityFlag_SURF = false, scaleValidityFlag_FF = false;
            float percentFG_SURF=0, percentFG_FF = 0;
            cv::Rect boundRect_SURF;

            float ARerr_FF = 0, scale_FF = 0;
            float ARerr_SURF = 0, scale_SURF = 0;

#if(GMM_DECIDE_ROI)
            bool gmmResult = false, scaleValidityFlag_GMM = false;
            float ARerr_GMM = 0, scale_GMM = 0, percentFG_GMM = filteredCandidatePercentFG[k1][i1];
#endif
            cv::Point2f cPoint;
            cv::Rect roiOut, roiIn(0,0,templateImg.cols,templateImg.rows);
            double percentMatch = surfMatching(desc1,desc2,kp1,kp2,templateImg,queryImg,cPoint,roiOut);


            if(percentMatch > 0)
            {
                // Test if product differs in orientation
                bool rotateFlag = false;
                if(templateImg.cols > templateImg.rows && roiOut.width > roiOut.height)
                {
                    rotateFlag = false;
                }
                else if(templateImg.cols < templateImg.rows && roiOut.width < roiOut.height)
                {
                    rotateFlag = false;
                }
                else
                {
                    rotateFlag = true;
                }

                roiOut.x = cPoint.x - roiOut.width/2;
                roiOut.y = cPoint.y - roiOut.height/2;
                bool testResult = verifyResultantRoi(roiIn,roiOut,ARerr_SURF,scale_SURF,rotateFlag);

                if(testResult)
                {
                    //                    cv::Mat templateHist = templateHistograms[foundProductIndex[k1]];

                    //                    cv::Mat roiHist;
                    //                    computeColorDistributionRGB(queryImgHSV,roiOut,roiHist);

                    //                    double bhattCoeff = 0;

                    //                    for(int k1=0; k1<templateHist.cols; k1++)
                    //                    {
                    //                        bhattCoeff += sqrt(templateHist.at<float>(0,k1)*roiHist.at<float>(0,k1));
                    //                    }

//                    cv::Mat outImg;

//                    cv::Rect roiTemp = roiOut;
//                    checkBoundary(queryImg,roiTemp);

//                    if(roiTemp.area() != 0)
//                    {
//                        bool productFound = productRecognise(queryImg(roiTemp).clone(),emModelTemplate[foundProductIndex[k1]],
//                                                             emModelBG,percentFG_SURF,outImg,boundRect_SURF);
//                        boundRect_SURF.x += roiTemp.x;
//                        boundRect_SURF.y += roiTemp.y;
//                    }

                    surfResult = true;

                    float scaleValidity = scale_SURF/validScaleVal;
                    if(scaleValidity > 1.3 || scaleValidity < 0.7)
                        scaleValidityFlag_SURF = false;
                    else
                        scaleValidityFlag_SURF = true;

                }

            }

            // Use Flood Fill to find out an ROI

            vector<cv::Point2f> queryPts, queryPtsFiltered; cv::KeyPoint::convert(kp2MatchedTemplateOnly,queryPts);
            float pff;
//            for(int k3=0; k3<queryPts.size(); k3++)
//            {
//                bool ok = pointRecognise(queryImgHSV,queryPts[k3],emModelTemplate[foundProductIndex[k1]],emModelBG);
//                if(ok)
//                    queryPtsFiltered.push_back(queryPts[k3]);
//            }

            cv::Rect roiOutFF = findRoifloodFill(queryImg,queryPts,testRoi,pff);

            if(roiOutFF.area() != 0)
            {
                bool testResult = verifyResultantRoi(roiIn,roiOutFF,ARerr_FF,scale_FF);

                if(testResult)
                {
                    //                    cv::Mat templateHist = templateHistograms[foundProductIndex[k1]];

                    //                    cv::Mat roiHist;
                    //                    computeColorDistributionRGB(queryImgHSV,testRoi,roiHist);

                    //                    double bhattCoeff = 0;

                    //                    for(int k1=0; k1<templateHist.cols; k1++)
                    //                    {
                    //                        bhattCoeff += sqrt(templateHist.at<float>(0,k1)*roiHist.at<float>(0,k1));
                    //                    }

//                    cv::Mat outImg;
//                    cv::Rect roiTemp = roiOutFF,boundRect;
//                    checkBoundary(queryImg,roiTemp);
//                    if(roiTemp.area() != 0)
//                    {
//                        bool productFound = productRecognise(queryImg(roiTemp).clone(),emModelTemplate[foundProductIndex[k1]],
//                                                             emModelBG,percentFG_FF,outImg,boundRect);
//                    }

                    floodFillResult = true;

                    float scaleValidity = scale_FF/validScaleVal;
                    if(scaleValidity > 1.3 || scaleValidity < 0.7)
                        scaleValidityFlag_FF = false;
                    else
                        scaleValidityFlag_FF = true;
                }
            }


#if(GMM_DECIDE_ROI)
            // Use GMM to find an ROI
            if(gmmRoi.area() != 0)
            {
                bool testResult = verifyResultantRoi(roiIn,gmmRoi,ARerr_GMM,scale_GMM);

                if(testResult)
                {
                    gmmResult = true;

                    float scaleValidity = scale_GMM/validScaleVal;
                    if(scaleValidity > 1.3 || scaleValidity < 0.7)
                        scaleValidityFlag_GMM = false;
                    else
                        scaleValidityFlag_GMM = true;
                }
            }
#endif

#if(COLOR_GRAPH)
            cv::Rect roiTemp = testRoi;
            checkBoundary(queryImg,roiTemp);
            //            findTargetCandidates(queryImg(roiTemp).clone(),imgMeansTemp,15,targetCandidates);

            vector<cv::Point2f> cc1, cc2;
            vector<cv::Vec3b> colorsTemp1,colorsTemp2;
            vector<cv::Rect> patchRois1, patchRois2;
            cv::Mat imgMeans, outImg1, outImg2;
            cv::Rect detectedRect = foundProductRois[k1][0];
            cv::Rect roiTemp3 = detectedRect;
            checkBoundary(queryImg,roiTemp3);

            floodFill_(queryImg(roiTemp3).clone(),cc1,colorsTemp1,patchRois1,outImg1);
            floodFill_(queryImg(roiTemp).clone(),cc2,colorsTemp2,patchRois2,outImg2);

            //            cv::waitKey(0);

            vector<int> matchIndexTemp;
            cv::Rect roiTemp2;
            vector<cv::Point2f> roiPoints2;
            vector<cv::Point2f> mPointEllipse2;
            cv::Point2f cPointTemp;
            double scalingTemp;
            vector<cv::Point2f> mp2Temp;

            vector<cv::KeyPoint> kp1Temp,kp2Temp;
            cv::KeyPoint::convert(cc1,kp1Temp);
            cv::KeyPoint::convert(cc2,kp2Temp);

            cv::Mat desc1Temp, desc2Temp;
            for(int k2=0; k2<colorsTemp1.size(); k2++)
            {
                cv::Mat temp(1,3,CV_32FC1);
                temp.at<float>(0,0) = float(colorsTemp1[k2][0]);
                temp.at<float>(0,1) = float(colorsTemp1[k2][1]);
                temp.at<float>(0,2) = float(colorsTemp1[k2][2]);

                desc1Temp.push_back(temp);
            }
            for(int k2=0; k2<colorsTemp2.size(); k2++)
            {
                cv::Mat temp(1,3,CV_32FC1);
                temp.at<float>(0,0) = float(colorsTemp2[k2][0]);
                temp.at<float>(0,1) = float(colorsTemp2[k2][1]);
                temp.at<float>(0,2) = float(colorsTemp2[k2][2]);

                desc2Temp.push_back(temp);
            }

            vector< vector<int> > nbrIndexTemp;
            vector< vector<float> > nbrDists, nbrDists_mod;
            findKNN(desc1Temp,desc2Temp,3,nbrIndexTemp,nbrDists);


            vector< vector<cv::Point2f> > targetCandidates;
            vector<cv::Point2f> cc1_mod;
            for(int k2=0; k2<nbrIndexTemp.size(); k2++)
            {
                vector<cv::Point2f> candidatesTemp;
                vector<float> distTemp;
                cout << cv::Mat(nbrDists[k2]) << "\t" << (cv::Mat)nbrIndexTemp[k2] << endl;
                for(int j2=0; j2<nbrIndexTemp[k2].size(); j2++)
                {
                    int ind = nbrIndexTemp[k2][j2];
                    float dist = nbrDists[k2][ind];

                    cout << k2 << "\t" << j2 << "\t" << ind << endl;
                    float areaRatio = (float)patchRois2[ind].area()/(float)patchRois1[k2].area();

                    float areaErr = abs(1-areaRatio);

                    dist *= (20*areaErr);

//                    cout << ind << " \t dist \t" << dist << endl;
//                    if(dist < 40)
                        candidatesTemp.push_back(cc2[ind]);
                        distTemp.push_back(dist);
                }

                if(!candidatesTemp.empty())
                {
                    cc1_mod.push_back(cc1[k2]);
                    targetCandidates.push_back(candidatesTemp);
                    nbrDists_mod.push_back(distTemp);
                    cout << cv::Mat(candidatesTemp) << endl;
                }
            }


            if(cc1_mod.size() > 1)
            {
                cv::Mat_<int> adjMat(cc1_mod.size(), cc1_mod.size(), -1);
                vector< vector<int> > nbrList(cc1_mod.size());
                draw_subdiv(outImg1,cc1_mod,adjMat,nbrList);

                cv::imshow("srcGraph",outImg1);

                vector<cv::Point2f> outPoints;
                vector<int> partsFound;
                vector<int> matchIndex;

                for(int k3=0; k3<nbrIndexTemp.size(); k3++)
                    cout << (cv::Mat)nbrIndexTemp[k3] << endl;

                cout << adjMat << endl;

                findMatch_MRF_MAP(adjMat,cc1_mod,nbrDists_mod,targetCandidates,outPoints,partsFound,matchIndex);

                for(int k2=0; k2<cc2.size(); k2++)
                {
                    cv::circle(outImg2,cc2[k2],2,cv::Scalar(255,0,255),2);
                    char txt[20];
                    sprintf(txt,"%d",k2);
                    cv::putText(outImg2,txt,cc2[k2],1,1,cv::Scalar(0,255,0),1);
                }

                for(int k2=0; k2<outPoints.size(); k2++)
                {
                    cv::circle(outImg2,outPoints[k2],2,cv::Scalar(0,0,255),2);
//                    char txt[20];
//                    sprintf(txt,"%d",k2);
//                    cv::putText(outImg2,txt,outPoints[k2],1,1,cv::Scalar(0,255,0),1);
                }

                int edgeCounter = 0;
                for(uint i=0; i<adjMat.rows; i++)
                    for(uint j=i+1; j<adjMat.cols; j++)
                    {
                        if(adjMat[i][j]!=-1)
                        {
                            if(partsFound[edgeCounter] == 1)
                            {
                                cv::line(outImg2,targetCandidates[i][matchIndex[i]],targetCandidates[j][matchIndex[j]],
                                         cv::Scalar(0,255,255),2);
                            }
                            edgeCounter++;
                        }
                    }

//                surfMatching(desc1Temp,desc2Temp,kp1Temp,kp2Temp,outImg1,outImg2,cPointTemp,roiTemp2);
                cv::imshow("queryGraph",outImg2);
                cv::waitKey(0);
            }
#endif

            // Decide a window from all the above results

            if(surfResult && floodFillResult)
            {
                float overlap = 0;
                cv::Rect r1 = roiOut;
                cv::Rect r2 = roiOutFF;

                checkBoundary(queryImg,r1);
                checkBoundary(queryImg,r2);

                calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);

                float percentOverlap1 = overlap/(r1.area());
                float percentOverlap2 = overlap/(r2.area());
                if(percentOverlap1 > 0.3 || percentOverlap2 > 0.3)
                {
                    float conf1 = percentFG_SURF + (1-abs(1-ARerr_SURF));
                    float conf2 = percentFG_FF + (1-abs(1-ARerr_FF));

                    if(conf1 > conf2)
                    {
                        floodFillResult = false;
                    }
                    else
                    {
                        surfResult = false;
                    }
                }

            }

            bool lastOptionGMM = false;
            if(surfResult)
            {
                if(scaleValidityFlag_SURF)
                {
                    finalBCs[k1].push_back(percentFG_SURF);
                    finalARerr[k1].push_back(ARerr_SURF);
                    finalScale[k1].push_back(scale_SURF);

                    //                    roiOut = boundRect_SURF;
                    foundProductRois[k1].push_back(roiOut);
                    candidateNbrTestIndex[k1].push_back(1);
                }
                else
                    lastOptionGMM = true;
            }
//            else if(floodFillResult)
//            {
//                if(scaleValidityFlag_FF)
//                {
//                    finalBCs[k1].push_back(percentFG_FF);
//                    finalARerr[k1].push_back(ARerr_FF);
//                    finalScale[k1].push_back(scale_FF);

//                    foundProductRois[k1].push_back(roiOutFF);
//                    candidateNbrTestIndex[k1].push_back(1);
//                }
//                else
//                    lastOptionGMM = true;
//            }

#if(GMM_DECIDE_ROI)
            if(lastOptionGMM)
            {
                finalBCs[k1].push_back(percentFG_GMM);
                finalARerr[k1].push_back(ARerr_GMM);
                finalScale[k1].push_back(scale_GMM);

                foundProductRois[k1].push_back(gmmRoi);
                candidateNbrTestIndex[k1].push_back(1);
            }
#endif

        }
    }

}


void connectedComponent_GMM(cv::Mat input, cv::Mat mask, cv::Point2f seed, cv::Vec3b color, cv::EM emModelFG, cv::EM emModelBG)
{
    cv::Rect imgBound(0,0,input.cols,input.rows);

    if(!imgBound.contains(seed))
        return;

    bool fg = pointRecognise(input,seed,emModelFG,emModelBG);

    vector<cv::Point2f> queue;
    vector<cv::Point2f> sign;
    sign.push_back(cv::Point2f(-1,-1));
    sign.push_back(cv::Point2f(0,-1));
    sign.push_back(cv::Point2f(1,-1));
    sign.push_back(cv::Point2f(-1,0));
    sign.push_back(cv::Point2f(1,0));
    sign.push_back(cv::Point2f(-1,1));
    sign.push_back(cv::Point2f(0,1));
    sign.push_back(cv::Point2f(1,1));

    if(fg)
    {
        input.at<cv::Vec3b>(seed.y,seed.x) = color;
        queue.push_back(seed);

        while(!queue.empty())
        {
            cv::Point2f pt1 = queue[queue.size()-1];
            queue.pop_back();

            for(int i=0; i<sign.size(); i++)
            {
                cv::Point2f pt1Nbr = pt1 + sign[i];

                if(!imgBound.contains(pt1Nbr))
                    continue;

                cv::Vec3b nbrVal = input.at<cv::Vec3b>(pt1Nbr.y,pt1Nbr.x);

                if(mask.at<cv::Vec3b>(pt1Nbr.y,pt1Nbr.x) != cv::Vec3b(0,0,0) || nbrVal == color)
                    continue;

                bool fgNbr = pointRecognise(input,pt1Nbr,emModelFG,emModelBG);

                if(fgNbr)
                {
                    input.at<cv::Vec3b>(pt1Nbr.y,pt1Nbr.x) = color;
                    queue.push_back(pt1Nbr);
                }
                else
                {
                    continue;
                }
            }
        }
    }
    else
    {
        return;
    }
}

void findSimialarNbr_GMM(cv::Mat queryImg, vector< vector<cv::Rect> > rois, vector<int> productIndicies,
                         vector<cv::EM> emModelFG, cv::EM emModelBG)
{
    cv::Mat input;
    cv::cvtColor(queryImg,input,CV_BGR2HSV);

    cv::Mat ccImg = input.clone();
    cv::Vec3b color(0,0,0);

    cv::Mat mask = cv::Mat::zeros(input.rows,input.cols,CV_8UC3);

    for(int i=0; i<rois.size(); i++)
        for(int j=0; j<rois[i].size(); j++)
        {
            checkBoundary(input,rois[i][j]);
            ccImg(rois[i][j]).copyTo(mask(rois[i][j]));
        }

    for(int i=0; i<rois.size(); i++)
    {
        int productIndex = productIndicies[i];

        for(int j=0; j<rois[i].size(); j++)
        {
            cv::Rect r1 = rois[i][j];

            // Top Edge
            for(int k=r1.x; k<r1.br().x; k+=7)
            {
                cv::Point2f p1(k,r1.y-1);
                connectedComponent_GMM(ccImg,mask,p1,color,emModelFG[productIndex],emModelBG);
            }

            // Right Edge
            for(int k=r1.y; k<r1.br().y; k+=7)
            {
                cv::Point2f p1(r1.x+1,k);
                connectedComponent_GMM(ccImg,mask,p1,color,emModelFG[productIndex],emModelBG);
            }

            // Bottom Edge
            for(int k=r1.x; k<r1.br().x; k+=7)
            {
                cv::Point2f p1(k,r1.y+1);
                connectedComponent_GMM(ccImg,mask,p1,color,emModelFG[productIndex],emModelBG);
            }

            // Left Edge
            for(int k=r1.y; k<r1.br().y; k+=7)
            {
                cv::Point2f p1(r1.x-1,k);
                connectedComponent_GMM(ccImg,mask,p1,color,emModelFG[productIndex],emModelBG);
            }
        }
    }

    cv::Mat outImg = queryImg.clone();

    for(int i=0; i<ccImg.rows; i++)
        for(int j=0; j<ccImg.cols; j++)
        {
            if(ccImg.at<cv::Vec3b>(i,j) == color)
            {
                outImg.at<cv::Vec3b>(i,j) = color;
            }
        }

    cv::imshow("neqw",outImg);
    cv::waitKey(0);
}


void findPatternMatch_Graph(cv::Mat templateImg, cv::Mat queryImg, vector<cv::Point2f> points1,
                            vector< vector<cv::Point2f> > points2Candidates)
{
    cv::Mat srcImg = templateImg.clone();
    cv::Mat targetImg = queryImg.clone();

    cv::Mat_<int> adjMat(points1.size(),points1.size(),-1);
    vector< vector<int> > nbrList(points1.size());
    draw_subdiv(srcImg,points1,adjMat,nbrList);

    cv::imshow("aiwen",srcImg);
    cv::waitKey(0);
}


void productCountRANSAC(cv::Mat queryImg, vector<int> candidateIndex, vector<int> matchingDescriptorIndices,
                        std::map<int , vector<int> > descriptorNodeTable, std::map<int, cv::KeyPoint> keyPointNodeTable,
                        vector<cv::KeyPoint> queryKeypoint,cv::Mat queryDesc, vector<int> queryMatchInd, vector<string> imgFileNames,
                        vector<cv::Mat> templateHistograms, cv::VideoWriter writer2, vector<int>& itemCount,
                        cv::Mat finalImage,vector<cv::Point2f> &majMinAxes, vector<cv::RotatedRect> eTemplates,
                        vector< vector<cv::KeyPoint> > templateKeyPoints, vector<cv::Mat> templateDesc,
                        vector<cv::EM> emModelTemplate, cv::EM emModelBG, vector<int> pointTemplateIndex)
{
    vector<cv::Rect> finalRois; // Stores 'all' the finalized rois in a single vector
    vector< vector<cv::Point2f> > finalRoiPoints;   // Stores 'all' the final rois (points form) in a single vector

    cv::Mat queryImgHSV;
    cv::cvtColor(queryImg,queryImgHSV,CV_BGR2HSV);

    vector< vector<cv::Point2f> > imageMatchPointsForEllipse;

    vector<int> foundProductIndex;      // Stores the indicies of found products wrt the original template pool
    vector< vector<cv::Rect> > foundProductRois;    // Stores the ROIs of found products wrt the templates which are detected in current frame
    vector< vector<int> > candidateNbrTestIndex;    // Candidates on which neighborhood test has to be performed
    vector< vector<float> > finalBCs, finalARerr, finalScale; // Corresponding parameters for selected ROIs

    // Loop for all the candidate templates

    for(int i1=0; i1<candidateIndex.size(); i1++)
    {
        if (JUNK_OUTPUT)
            cout << "Initiate matching for template " << candidateIndex[i1] << endl;

        vector<cv::KeyPoint> kpTemplate, kpQuery;
        vector<int> kpTemplateIndex, kpQueryIndex;
        for(int k=0; k<matchingDescriptorIndices.size(); k++)
        {
            int templateIndex;
            templateIndex = descriptorNodeTable.at(matchingDescriptorIndices[k]).at(0);

            // if the product being considered has the same id as given by the matched descriptor
            if(templateIndex == candidateIndex[i1])
            {
                cv::KeyPoint kp1, kp2;
                int kp1Index = matchingDescriptorIndices[k];
                int kp2Index = queryMatchInd[k];
                kp1 = keyPointNodeTable.at(matchingDescriptorIndices[k]);
                kp2 = queryKeypoint[queryMatchInd[k]];

                kpTemplate.push_back(kp1);
                kpQuery.push_back(kp2);

                kpTemplateIndex.push_back(kp1Index);
                kpQueryIndex.push_back(kp2Index);
            }

        }

        vector<int> kpTemplateUniqueIndex;  // Only stores non-repeated keyPoints
        vector< vector<cv::KeyPoint> > kpQueryCandidates;
        // keep only the unique kpTemplate
        for(int k1=0; k1<kpTemplateIndex.size(); k1++)
        {
            vector<cv::KeyPoint> kpQueryTemp;
            bool toAdd = true;
            for(int j1=0; j1<kpTemplateUniqueIndex.size(); j1++)
            {
                if(kpTemplateIndex[k1]==kpTemplateUniqueIndex[j1])
                {
                    toAdd = false;
                    kpQueryCandidates[j1].push_back(queryKeypoint[kpQueryIndex[k1]]);
//                    break;
                }
            }

            if(toAdd)
            {
                kpTemplateUniqueIndex.push_back(kpTemplateIndex[k1]);
                kpQueryTemp.push_back(queryKeypoint[kpQueryIndex[k1]]);
                kpQueryCandidates.push_back(kpQueryTemp);
            }
        }

        vector<cv::KeyPoint> kpTemplateUnique;
        for(int k1=0; k1<kpTemplateUniqueIndex.size(); k1++)
            kpTemplateUnique.push_back(keyPointNodeTable.at(kpTemplateUniqueIndex[k1]));

        vector<cv::Point2f> points1Unique;
        cv::KeyPoint::convert(kpTemplateUnique,points1Unique);

        vector< vector<cv::Point2f> > kpQueryCandidatesPts(kpQueryCandidates.size());
        for(int k1=0; k1<kpQueryCandidates.size(); k1++)
        {
            cv::KeyPoint::convert(kpQueryCandidates[k1],kpQueryCandidatesPts[k1]);
        }

        cv::Mat templateImg = cv::imread(imgFileNames[candidateIndex[i1]]);

        // Calculate source histogram

        cv::Rect templateRect(0,0,templateImg.cols,templateImg.rows);
        cv::Mat templateHist, templateImgHSV;
        cv::cvtColor(templateImg,templateImgHSV,CV_BGR2HSV);
        computeColorDistributionRGB(templateImgHSV,templateRect,templateHist);


        vector<cv::Point2f> points1,points2;
        cv::KeyPoint::convert(kpTemplate,points1);
        cv::KeyPoint::convert(kpQuery,points2);

        cv::Rect roi;
        vector<int> matchIndex;

        vector<cv::Rect> foundProductRoisTemp;
        vector<int> candidateNbrTestIndexTemp;
        vector<float> finalBCsTemp, finalARerrTemp, finalScaleTemp;

        int matchCount;
        // Iterate for a maximum of 20 times to detect all instances of a template product
        for(int j=0; j<20; j++)
        {
            // Minimum number of points required to continue
            if(points2.size() < 4)
            {
//                cout << "Exiting Loop as point pool depleted" << endl;
                break;
            }

            vector<cv::Point2f> points1_,points2_;

            // Call the RANSAC routine for point pattern matching

            matchIndex.clear();
            vector<cv::Point2f> roiPoints;
            vector<cv::Point2f> mPoints;
            cv::Point2f centerPoint;
            double scaling_value = 0.0;
            vector<cv::Point2f> mp2;
//            cout << 1 << "\t" << ros::Time::now() << endl;
            findPatternMatch(templateImg,queryImg,points1,points2,matchIndex, roi,roiPoints,mPoints,centerPoint,
                             scaling_value,mp2);
//            cout << 2 << "\t" << ros::Time::now() << endl;
//            findPatternMatch_Graph(templateImg,queryImg,points1Unique,kpQueryCandidatesPts);


#if(POINT_RECT)
            roi = cv::boundingRect(roiPoints);
#endif
            if(j==0)
            {
                matchCount = matchIndex.size();
            }


            // Flags hold true if correspondig thresholds are not satisfied indicating poor matching
            bool ARflag=false, scaleFlag=false, histMatchFlag=false, percentFG_GMM_Flag = false;

            // Calculate Aspect Ratio and Scaling errors
            float AR = float(templateImg.cols) / float(templateImg.rows);

            // Test if product differs in orientation
            bool rotateFlag = false;
            if(templateImg.cols > templateImg.rows && roi.width > roi.height)
            {
                rotateFlag = false;
            }
            else if(templateImg.cols < templateImg.rows && roi.width < roi.height)
            {
                rotateFlag = false;
            }
            else
            {
                rotateFlag = true;
            }


            roi.x = centerPoint.x - roi.width/2;
            roi.y = centerPoint.y - roi.height/2;
            roi.width = roi.width;
            roi.height = roi.height;

#if(POINT_RECT)
            int widthRoi = norm(roiPoints[0]-roiPoints[1]);
            int heightRoi = norm(roiPoints[1]-roiPoints[2]);
            float ARnow = float(widthRoi) / float(heightRoi);
            float scale = float(widthRoi) / float(templateImg.cols);

#else
            float ARnow = 0.0,scale = 0.0;
            if(!rotateFlag)
            {
                ARnow = float(roi.width) / float(roi.height);
                scale = float(roi.width) / float(templateImg.cols);
            }
            else
            {
                ARnow = float(roi.height) / float(roi.width);
                scale = float(roi.height) / float(templateImg.cols);
            }
#endif


            float ARerr = AR/ARnow;

            if(ARerr > 1.3 || ARerr < 0.7)
            {
                ARflag = true;
            }

            if(scale < 0.5 || scale > 2 )
            {
                scaleFlag = true;
            }

            // Check major and minor axes of the product identified through RANSAC

            bool ellipseFlag = false;
            cv::RotatedRect rotRect;
            if(mPoints.size() > 2)
                rotRect = cv::minAreaRect(mPoints);

            double majorAxis = 0.0, minorAxis = 0.0;
            if(rotRect.size.height >= rotRect.size.width)
            {
                majorAxis = rotRect.size.height;
                minorAxis = rotRect.size.width;
            }
            else
            {
                majorAxis = rotRect.size.width;
                minorAxis = rotRect.size.height;

            }

            if(majorAxis > majMinAxes[candidateIndex[i1]].x)
            {
                ellipseFlag = true;
            }
            else if(minorAxis > majMinAxes[candidateIndex[i1]].y)
            {
                ellipseFlag = true;
            }


            // Calculate histogram matching

            cv::Mat roiHist, queryImgHSV;
            cv::cvtColor(queryImg,queryImgHSV,CV_BGR2HSV);

            computeColorDistributionRGB(queryImgHSV,roi,roiHist);

            double bhattCoeff = 0;

            for(int k1=0; k1<templateHist.cols; k1++)
            {
                bhattCoeff += sqrt(templateHist.at<float>(0,k1)*roiHist.at<float>(0,k1));
            }

            if(bhattCoeff < 0.4)
            {
                histMatchFlag = true;
            }
#if(PRODUCT_RECO_GMM)
            float percentFG=0,pFG_FF = 0;
            cv::Mat tempOutImg;
            cv::Rect roiTemp = roi,boundRect;
            checkBoundary(queryImg,roiTemp);

            if(roiTemp.area() != 0)
            {
                cout << 1 << "\t" << ros::Time::now() << endl;
                bool prodReco = productRecognise(queryImg(roiTemp).clone(),emModelTemplate[candidateIndex[i1]],emModelBG,
                                                 percentFG,tempOutImg,boundRect);
                cout << 2 << "\t" << ros::Time::now() << endl;

                boundRect.x += roiTemp.x;
                boundRect.y += roiTemp.y;
            }

            if(percentFG < 0.5)
            {
                percentFG_GMM_Flag = true;
            }
#endif

            // First check the histogram matching, then scaling and aspect ratio
            //            if(!histMatchFlag)
            if(!percentFG_GMM_Flag && !histMatchFlag)
            {
                if(!scaleFlag && !ARflag && !ellipseFlag)
                {
                    if(JUNK_OUTPUT)
                    {
                        cout << "Matched Template " << candidateIndex[i1] << " with AR and scale " << ARerr <<
                                "\t" << scale << "\t" << bhattCoeff
#if(PRODUCT_RECO_GMM)
                             << "\t" << percentFG
#endif
                                << endl;
                    }

                    imageMatchPointsForEllipse.push_back(mPoints);

                    //                    roi = boundRect;

                    foundProductRoisTemp.push_back(roi);
                    candidateNbrTestIndexTemp.push_back(1);
#if(PRODUCT_RECO_GMM)
                    finalBCsTemp.push_back(percentFG);
#else
                    finalBCsTemp.push_back(bhattCoeff);
#endif
                    finalARerrTemp.push_back(ARerr);
                    finalScaleTemp.push_back(scale);

                    finalRois.push_back(roi);
                    finalRoiPoints.push_back(roiPoints);
                }
                else
                {
                    if(JUNK_OUTPUT)
                    {
                        cout << "Unmatched Template " << candidateIndex[i1] << " with AR and scale " << ARerr <<
                                "\t" << scale << "\t" << bhattCoeff
#if(PRODUCT_RECO_GMM)
                             << "\t" << percentFG
#endif
                                << endl;
                    }

                    // Templates which didn't match in the very first iteration, need not be matched again
                    if(j==0)
                        break;
                }
            }
            else
            {
                if(JUNK_OUTPUT)
                {
                    cout << "Unmatched Template " << candidateIndex[i1] << " with AR and scale " << ARerr <<
                            "\t" << scale << "\t" << bhattCoeff
#if(PRODUCT_RECO_GMM)
                         << "\t" << percentFG
#endif
                            << endl;
                }

                // Templates which didn't match in the very first iteration, need not be matched again
                if(j==0)
                    break;
            }


            // Remove points from the pool corresponding to which product has already been matched
            int tempCounter=0;
            for(int i=0; i<points2.size(); i++)
            {
                // Remove points from inside the detected ROI
                bool allocated = false;
                for(int k1=0; k1<finalRois.size(); k1++)
                {
                    if(finalRois[k1].contains(points2[i]))
                    {
                        allocated = true;
                        break;
                    }
                }

                if(matchIndex.size() > 0)
                {
                    if(matchIndex[tempCounter] != i && !allocated)
                    {
                        points2_.push_back(points2[i]);
                        points1_.push_back(points1[i]);
                    }
                    else
                        tempCounter++;
                }
            }

            points1.clear();
            points2.clear();
            for(int i=0; i<points2_.size(); i++)
            {
                points1.push_back(points1_[i]);
                points2.push_back(points2_[i]);
            }

            // At least these many points are needed to successfully detect an object
            if(points2.size() < 0.2 * matchCount)
            {
//                cout << "Exiting Loop as point pool is too small in size" <<  points2.size() <<  endl;
                break;
            }
        }

        if(!foundProductRoisTemp.empty())
        {
            foundProductRois.push_back(foundProductRoisTemp);
            candidateNbrTestIndex.push_back(candidateNbrTestIndexTemp);
            finalBCs.push_back(finalBCsTemp);
            finalARerr.push_back(finalARerrTemp);
            finalScale.push_back(finalScaleTemp);

            foundProductIndex.push_back(candidateIndex[i1]);

        }
    }


    filterOverlappingRois(queryImg,finalBCs,finalARerr,finalScale,foundProductIndex,foundProductRois,candidateNbrTestIndex);
    cout << "Unique Products Identified = " << foundProductIndex.size() << "\t" << (cv::Mat)foundProductIndex << endl;

    //    findSimialarNbr_GMM(queryImg,foundProductRois,foundProductIndex,emModelTemplate,emModelBG);

    while(1)
    {
        int lastItemCount =0;
        for(int k1=0; k1<foundProductRois.size(); k1++)
            lastItemCount += foundProductRois[k1].size();

//        cout << 1 << ros::Time::now() << endl;
        findNeighbourProducts(queryImg,templateHistograms,imgFileNames,queryKeypoint,queryDesc,templateKeyPoints,templateDesc,
                              emModelTemplate,emModelBG,pointTemplateIndex,foundProductIndex,foundProductRois,finalBCs,
                              finalARerr,finalScale,candidateNbrTestIndex);
//        cout << 2 << ros::Time::now() << endl;

        filterOverlappingRois(queryImg,finalBCs,finalARerr,finalScale,foundProductIndex,foundProductRois,candidateNbrTestIndex);

        int newItemCount =0;
        for(int k1=0; k1<foundProductRois.size(); k1++)
            newItemCount += foundProductRois[k1].size();

        int addedItems = newItemCount-lastItemCount;

        cout << "Added " << addedItems << " more items" << endl;

        if(addedItems == 0)
            break;
    }


    // Display final data
    for(int i1=0; i1<foundProductRois.size(); i1++)
        for(int j1=0; j1<foundProductRois[i1].size(); j1++)
        {
            itemCount[foundProductIndex[i1]]++;

            cv::Rect r1 = foundProductRois[i1][j1];

#if(POINT_RECT)
            drawRectangle(queryImg,finalRoiPoints[k],cv::Scalar(255,0,0),2);
            drawRectangle(finalImage,finalRoiPoints[k],cv::Scalar(255,0,0),2);

            cv::rectangle(queryImg,rectVector[k],cv::Scalar(0,255,0),2,8);
            cv::rectangle(finalImage,rectVector[k],cv::Scalar(0,255,0),2,8);
#else
            cv::rectangle(queryImg,r1,cv::Scalar(255,0,0),2);
            cv::rectangle(finalImage,r1,cv::Scalar(255,0,0),2);
#endif

            char txt[50];
            sprintf(txt,"%d",foundProductIndex[i1]);
            cv::putText(queryImg,txt,0.5*(r1.tl()+r1.br()),1,2,cv::Scalar(0,255,255),2);
        }

//    imshow("query",queryImg);
//    cv::waitKey(0);
    writer2.write(queryImg);
}


/*!
  @brief This function does one time calculation required for productCounting
            like building kdtree over templates and maintaining a inverted index
            table for lookup.

  @return void.
  */

void initProductCount(vector<string> &imgFileNames,std::map<int , vector<int> > &descriptorNodeTable,
                      std::map<int, cv::KeyPoint> &keyPointNodeTable,cv::Mat &dataSet,vector<int> &descriptorCount,
                      vector<cv::Point2f> &majorMinorAxesOfTemplates, vector<cv::RotatedRect> &ellipseTemplates,
                      vector<cv::Mat> &templateHistograms,vector< vector<cv::KeyPoint> > &templateKeypoints,
                      vector<  cv::Mat > &templateDescriptors,cv::EM &emModelBG,vector<cv::EM> &emModelTemplate,
                      vector<int> &totalProductsDetected)
{
    /*
      Reading templates full filenames and storing them in a vector
      */
//    vector<string> imgFileNames
    for(int i=0;i<int(NO_OF_TEMPLATES);i++)
    {
        char imageName[200];
        sprintf(imageName,"/opt/rce/packages/productcount_ros/train/fg/%d.png",i);
        imgFileNames.push_back(imageName);
    }

    /*
      Inverted index implementation- This is used to find the template in which the matched descriptor is present.
      */
//    std::map<int , vector<int> > descriptorNodeTable;

    /*
      Index table for matching keypoint extraction
      */
//    std::map<int, cv::KeyPoint> keyPointNodeTable;

    /*
      Dataset consisting of descriptors extracted from all the templates
      */
//    cv::Mat dataSet;

    /*
      Vector storing the number of descriptors in each template
      */

//    vector<int> descriptorCount;

    /*
      Vector storing major and minor axis for each template
      */
//    vector<cv::Point2f> majorMinorAxesOfTemplates;


    /*
      Vector storing ellipse for each template
      */
//    vector<cv::RotatedRect> ellipseTemplates;

    /*
        Read all templates, detect keypoints, extract descriptors and store points in dataset which
        will be used to build a kdtree. Also maintain a inverted index table which will be used to
        find the templates in which a particular matched descriptor is present.
      */

//    vector<cv::Mat> templateHistograms;
//    vector< vector<cv::KeyPoint> > templateKeypoints(int(NO_OF_TEMPLATES));
//    vector<  cv::Mat > templateDescriptors;

    templateKeypoints = vector< vector<cv::KeyPoint> >(NO_OF_TEMPLATES);

    // Train the GMM model for the background
    emModelBG= cv::EM(GMM_CLUSTERS, CvEM::COV_MAT_DIAGONAL, cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 300, 0.1));

    // Train if not already trained
//    if(!GMM_TRAINED)
//    {
//        trainGMMbg(emModelBG);
//    }
//     else read them from the file.
//    else
//    {
//        cv::FileStorage bgModel("/opt/rce/packages/productcount_ros/models/bgModel.yaml",cv::FileStorage::READ);
//        const cv::FileNode& fnBg = bgModel["StatModel.EM"];
//        emModelBG.read(fnBg);
//        bgModel.release();
//    }


//    vector<cv::EM> emModelTemplate(NO_OF_TEMPLATES);
         emModelTemplate = vector<cv::EM>(NO_OF_TEMPLATES);


    for(int i=0;i<int(NO_OF_TEMPLATES);i++)
    {
        cv::Mat imgDescriptor;
        vector<cv::KeyPoint> imgKeypoints;

#if(OPENCV_FEATURE_DETECTOR)
        cv::Mat img = cv::imread(imgFileNames[i]);
        detector->detect(img,imgKeypoints);
        descriptorExtractor->compute(img,imgKeypoints,imgDescriptor);
#else


        IplImage *img1;
        img1 = cvLoadImage(imgFileNames[i].c_str());

        IpVec ipts1;
        surfDetDes(img1,ipts1,false,4,4,2,0.0001f);

        for(int j=0;j<ipts1.size();j++)
        {
            cv::Mat tempMat = cv::Mat(1,64,CV_32FC1,0.0);
            cv::KeyPoint tempKey;
            for(int k=0;k<64;k++)
            {
                tempMat.at<float>(0,k) = ipts1[j].descriptor[k];
                tempKey.angle = ipts1[j].orientation;
                tempKey.pt.x = ipts1[j].x;
                tempKey.pt.y = ipts1[j].y;

            }
            imgDescriptor.push_back(tempMat);
            imgKeypoints.push_back(tempKey);
            tempMat.release();

        }

        cv::Mat img = cv::Mat(img1);

#endif
        vector<cv::Point2f> pointsTemp; cv::KeyPoint::convert(imgKeypoints, pointsTemp);
        cv::RotatedRect tempRect = cv::minAreaRect(pointsTemp);
//        if(i==5)
        /*
          160-180 width is width and height is height
          60-90 width is width and height is height
          0-60 width is height and height is width
          90-120 width is height and height is width

          */
//        {
//            cout << tempRect.size.width << "\t" << tempRect.size.height << "\t" << printAngle(tempRect) << endl;

//            cv::Mat imgi = cv::imread(imgFileNames[i]);
//            cv::ellipse(imgi,tempRect,cv::Scalar(255,0,0),2);
//            cv::imshow("1",imgi);
//            cv::waitKey(0);
////            exit(0);
//        }
        ellipseTemplates.push_back(tempRect);

        if(tempRect.size.height >= tempRect.size.width)
        {
            cv::Point2f ptf;
            ptf.x = tempRect.size.height;
            ptf.y = tempRect.size.width;
            majorMinorAxesOfTemplates.push_back(ptf);
        }
        else
        {
            cv::Point2f ptf;
            ptf.x = tempRect.size.width;
            ptf.y = tempRect.size.height;
            majorMinorAxesOfTemplates.push_back(ptf);
        }

        descriptorCount.push_back(imgDescriptor.rows);

        //        if(JUNK_OUTPUT)
        cout << "Template descriptors \t" << i << "\t" << imgDescriptor.rows << endl;

        vector<int> tempVector;
        tempVector.push_back(i);
        for(int j=0;j<imgDescriptor.rows;j++)
        {
            descriptorNodeTable.insert(std::pair<int,vector<int> >(j+dataSet.rows,tempVector));
            keyPointNodeTable.insert(std::pair<int,cv::KeyPoint>(j+dataSet.rows,imgKeypoints[j]));
            templateKeypoints[i].push_back(imgKeypoints[j]);
        }

        templateDescriptors.push_back(imgDescriptor);

        // Calculate source histogram
        cv::Rect templateRect(0,0,img.cols,img.rows);
        cv::Mat templateHist, templateImgHSV;
        cv::cvtColor(img,templateImgHSV,CV_BGR2HSV);
        computeColorDistributionRGB(templateImgHSV,templateRect,templateHist);
        templateHistograms.push_back(templateHist);

        appendMatrix(dataSet,imgDescriptor);
        tempVector.clear();


        emModelTemplate[i] = cv::EM(GMM_CLUSTERS, CvEM::COV_MAT_DIAGONAL, cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 300, 0.1));

        char emModelName[100];
        sprintf(emModelName,"/opt/rce/packages/productcount_ros/models/fgModel_%d.yaml",i);

        // Train the GMM model if not already trained
//        if(!GMM_TRAINED)
//        {
//            trainGMMfg(img1,emModelTemplate[i]);

//            cout << "Training Completed for template - " << i << endl;

//            cv::FileStorage fgModel(emModelName,cv::FileStorage::WRITE);
//            emModelTemplate[i].write(fgModel);
//            fgModel.release();
//        }
        // else read that from the file.
//        else
//        {
//            cv::FileStorage fgModel(emModelName, cv::FileStorage::READ);
//            const cv::FileNode& fnFg = fgModel["StatModel.EM"];
//            emModelTemplate[i].read(fnFg);
//            fgModel.release();
//        }


    }

    //    exit(0);

    /*
      Vector containing total products detected till that frame;
      */
//    vector<int> totalProductsDetected(int(NO_OF_TEMPLATES),0);
    totalProductsDetected = vector<int>(int(NO_OF_TEMPLATES),0);



    /*
      Initialse a kdtree parameters
      */
    cv::flann::KDTreeIndexParams kdparams(1);
//     kdparams = cv::flann::KDTreeIndexParams(1);


    /*
      Initialse a kdtree and build it with the dataset constructed through templates
      */

//    cv::flann::Index index(dataSet,kdparams,cvflann::FLANN_DIST_L2);
//      index1 = cv::flann::Index(dataSet,kdparams,cvflann::FLANN_DIST_L2);
     index1.build(dataSet,kdparams,cvflann::FLANN_DIST_L2);

//      cout << "Done" << endl;
//      exit(0);

    return;
}

void identifyAndCountProductsInImage(cv::Mat frame,vector<int> &cOnProducts)
{
    // Matrix for extracting the loop closure candidates after kdtree search based on matched features per node
    vector<int> neigbourResultVector(int(NO_OF_TEMPLATES),0);

    //Vector for storing the frequency of each descriptor;
    vector<int> descriptorFrequency(dataSet.rows,0.0);

    //Vector storing number of each type of prodcuts in each frame which has 1 item

    vector<int> temp(int(NO_OF_TEMPLATES),0);
    vector< vector<int> > productCount(int(MAX_SINGLE_PRODUCT_LIMIT),temp);

    //        cv::imshow("frame",frame);
    //        cv::waitKey(0);

    cv::Mat queryImg = frame.clone();
    cv::Mat resultImg = frame.clone();
    cv::Mat finalImage = frame.clone();



    // Query frame and descriptors and keypoints
    cv::Mat queryDesc;
    vector<cv::KeyPoint> queryKeypoint;


#if(OPENCV_FEATURE_DETECTOR)
            // Keypoint detection and descriptor extraction time in the query frame
            detector->detect(queryImg,queryKeypoint);
            descriptorExtractor->compute(queryImg,queryKeypoint,queryDesc);
#else

    IplImage queryCopy = queryImg;
    IplImage* imgQuery= &queryCopy;
    //        IplImage* imgQuery = frame.;

    IpVec iptsQuery;
    surfDetDes(imgQuery,iptsQuery,false,4,4,2,0.0001f);

    for(int j=0;j<iptsQuery.size();j++)
    {
        cv::Mat tempMat = cv::Mat(1,64,CV_32FC1,0.0);
        cv::KeyPoint tempKey;
        for(int k=0;k<64;k++)
        {
            tempMat.at<float>(0,k) = iptsQuery[j].descriptor[k];
            tempKey.angle = iptsQuery[j].orientation;
            tempKey.pt.x = iptsQuery[j].x;
            tempKey.pt.y = iptsQuery[j].y;

        }
        queryDesc.push_back(tempMat);
        queryKeypoint.push_back(tempKey);
        tempMat.release();

    }
#endif



    if(JUNK_OUTPUT)
        cout << "Number of descriptors \t" << queryDesc.rows << endl;

    // Number of matched descriptors in a query image using kdtree
    int matchedDescriptors = 0;


    //Vector constaining matching descriptors indices
    vector<int> matchingDescriptorIndices;

    vector<int> matchInd;

    vector<int> pointTemplateIndex(queryKeypoint.size(),-1); // Stores index of the template to which this query KeyPoint belongs

    /*
      Loop to calculate nearest neighbours for each descriptors and verifying them using distance threshold as a good match.
      */

    for(int descCount = 0; descCount < queryDesc.rows; descCount++)
    {
        // Matrices storing nearest neighbour index and their respective distances from they query descriptor
        cv::Mat indices,dists;
        // Number of nearest neighbours
        int knn = 2;
        // Initialsing search parameters for kdtree search
        const cv::flann::SearchParams& params=cv::flann::SearchParams(128);
        // Searching kdtree for nearest neighbour for each feature
        index1.knnSearch(cv::Mat(queryDesc.row(descCount)),indices,dists,knn,params);

        // Calculating distance ratio to decide whether the nearest neighbour is correct match or not
        float dist_ratio = float(dists.at<float>(0)/dists.at<float>(1));

        //Condition to check whether nearest neighbour obtained through kdtree is good or not
        if(dist_ratio < DISTANCE_RATIO)
        {

            matchingDescriptorIndices.push_back(indices.at<int>(0));
            matchInd.push_back(descCount);

            // Storing number of times same descriptor is obtained as a nearest neighbour in image
            descriptorFrequency[indices.at<int>(0)]+=1;

            matchedDescriptors++;

//             Showing matched descriptors on an image
//                cv::circle(finalImage,cv::Point(queryKeypoint[descCount].pt.x,queryKeypoint[descCount].pt.y),2,cv::Scalar(255,0,0),2,8,0);

            if(JUNK_OUTPUT)
                cout << "dist-ratio\t" << dist_ratio << endl;
//                            cv::imshow("result",resultImg);
//                            cv::waitKey(0);

            // Find all the nodes where this feature is present
            for(int j=0;j<descriptorNodeTable.at(indices.at<int>(0)).size();j++)
            {
                // Extract the node which contains this feature increment the number of features found in a particular node
                neigbourResultVector[descriptorNodeTable.at(indices.at<int>(0)).at(j)] += 1;

            }

            int templateIndex = descriptorNodeTable.at(indices.at<int>(0)).at(0);
            pointTemplateIndex[descCount] = templateIndex;

        }

        indices.release();
        dists.release();


    }

    // -----------------------------------------------------------------------------
    // -----------------------------------------------------------------------------
    // Begin point pattern matching
    // -----------------------------------------------------------------------------
    // -----------------------------------------------------------------------------


    /* This flag corresponds to the condition whether any product is detected in the frame or not */
    bool objectFound = false;
    vector<int> candidateIndex;

    for(int iter=0;iter<int(NO_OF_TEMPLATES);iter++)
    {
        double ratioDescMatchProduct = double(neigbourResultVector[iter])/descriptorCount[iter];
        //            if(neigbourResultVector[iter] > int(MIN_POINTS_FOR_OBJECT))
        if(ratioDescMatchProduct > OBJECT_THRESHOLD)
        {
            /* Variable storing number of products a particular type */
            candidateIndex.push_back(iter);
            objectFound = true;
        }
    }

    cout << "prodcut ransacc before\t" << matchInd.size() << endl;
    vector<int> itemCount(int(NO_OF_TEMPLATES),0);
    productCountRANSAC(frame,candidateIndex,matchingDescriptorIndices,descriptorNodeTable,keyPointNodeTable,queryKeypoint,
                       queryDesc,matchInd,imgFileNames,templateHistograms,writer2,itemCount,finalImage,majorMinorAxesOfTemplates,
                       ellipseTemplates,templateKeypoints,templateDescriptors,emModelTemplate,emModelBG,pointTemplateIndex);

    //Showing output in result Video
    for(int i=0;i<int(NO_OF_TEMPLATES);i++)
    {
        if(itemCount[i] > 0)
        {
           cOnProducts[i]+=itemCount[i];
        }
    }


}
